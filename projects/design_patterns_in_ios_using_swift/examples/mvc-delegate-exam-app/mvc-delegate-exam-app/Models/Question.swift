//
//  Question.swift
//  mvc-design-pattern
//
//  Created by Juan Francisco Dorado Torres on 2/21/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

struct Question {
    
    // MARK: Properties
    
    var text: String
    var point: Double
    var isCorrect: Bool
}
