//
//  ExamController.swift
//  mvc-design-pattern
//
//  Created by Juan Francisco Dorado Torres on 2/21/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ExamController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Properties
    
    private var questions: [Question] = [Question]()
    let questionsService = QuestionsService()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        self.questions = questionsService.getAll()
        self.tableView.reloadData()
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let nc = segue.destination as? UINavigationController else { fatalError() }
        guard let addQuestionVC = nc.viewControllers.first as? AddQuestionTableViewController else { fatalError() }
        addQuestionVC.delegate = self
    }
}

// MARK: UITableViewDelegate | UITableViewDataSource

extension ExamController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionTableViewCell", for: indexPath)
        
        cell.textLabel?.text = questions[indexPath.row].text
        
        return cell
    }
}

// MARK: - AddQuestionDelegate

extension ExamController: AddQuestionDelegate {
    
    func addQuestionDidSaveQuestion(question: Question, controller: UIViewController) {
        questionsService.add(question: question)
        controller.dismiss(animated: true)
        
        self.questions = questionsService.getAll()
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func addQuestionDidClose(controller: UIViewController) {
        controller.dismiss(animated: true)
    }
}
