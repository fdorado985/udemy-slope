//
//  AddQuestionTableViewController.swift
//  mvc-delegate-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/22/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

// MARK: - AddQuestionDelegate

protocol AddQuestionDelegate {
    func addQuestionDidSaveQuestion(question: Question, controller: UIViewController)
    func addQuestionDidClose(controller: UIViewController)
}

// MARK: - AddQuestionTableViewController

class AddQuestionTableViewController: UITableViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var questionTextField: UITextField!
    @IBOutlet weak var pointsTextField: UITextField!
    @IBOutlet weak var isCorrectSegmentedControl: UISegmentedControl!
    
    // MARK: Properties
    
    var delegate: AddQuestionDelegate!
    private var isCorrect: Bool = false
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isCorrectSegmentedControl.addTarget(self, action: #selector(isCorrectSegmentedControlSelected), for: .valueChanged)
    }
    
    // MARK: IBActions
    
    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        self.delegate.addQuestionDidClose(controller: self)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {
        guard let text = questionTextField.text,
            !text.isEmpty,
            let pointsString = pointsTextField.text,
            !pointsString.isEmpty,
            let points = Double(pointsString) else { return }
        
        let question = Question.init(text: text, point: points, isCorrect: isCorrect)
        self.delegate.addQuestionDidSaveQuestion(question: question, controller: self)
    }
    
    // MARK: Functions
    
    @objc func isCorrectSegmentedControlSelected(segmentedControl: UISegmentedControl) {
        self.isCorrect = segmentedControl.selectedSegmentIndex == 0 ? true : false
    }
}
