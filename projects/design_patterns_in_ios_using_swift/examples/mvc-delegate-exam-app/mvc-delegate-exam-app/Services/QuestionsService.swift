//
//  QuestionsService.swift
//  mvc-delegate-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/22/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class QuestionsService {
    
    private var questions = [Question]()
    
    func getAll() -> [Question] {
        return questions
//        return [
//            Question.init(text: "Is Earth Round", point: 10, isCorrect: true),
//            Question.init(text: "Is 2 + 2 = 4", point: 10, isCorrect: true),
//            Question.init(text: "Is 2 * 100 = 100", point: 10, isCorrect: false)
//        ]
    }
    
    func add(question: Question) {
        self.questions.append(question)
    }
}
