//
//  MainViewController.swift
//  mvvm-hello-maps
//
//  Created by Juan Francisco Dorado Torres on 2/24/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MainViewController: UIViewController {

    // MARK: IBOutlets
    
    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: Properties
    
    private let locationManager = CLLocationManager()
    let annotationFactory = AnnotationFactory()
    private let yelpService = YelpSearchService()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        
        self.mapView.showsUserLocation = true
    }

    // MARK: IBActions
    
    @IBAction func showAddAddressView(_ sender: UIBarButtonItem) {
        let alertVC = UIAlertController(title: "Enter Point of Interest", message: nil, preferredStyle: .alert)
        alertVC.addTextField(configurationHandler: nil)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            if let textField = alertVC.textFields?.first, let search = textField.text, !search.isEmpty {
                self.findNearbyPOI(by: search)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addAction(okAction)
        alertVC.addAction(cancelAction)
        
        self.present(alertVC, animated: true)
    }
    
    @IBAction func showYelpSuggestionsButtonTapped(_ sender: UIBarButtonItem) {
        yelpService.adaptSearchResultsFromYelp { (searchResults) in
            for business in searchResults.businesses {
                self.addBusinessToMap(business: business)
            }
        }
    }
    
    // MARK: Functions
    
    private func addBusinessToMap(business: Business) {
        let businessVM = annotationFactory.createBusinessViewModel(business: business)
        self.mapView.addAnnotation(businessVM)
    }
    
    private func findNearbyPOI(by searchTerm: String) {
        let annotations = self.mapView.annotations
        self.mapView.removeAnnotations(annotations)
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchTerm
        request.region = self.mapView.region
        
        let search = MKLocalSearch(request: request)
        search.start { (response, error) in
            guard let response = response, error == nil else { return }
            for mapItem in response.mapItems {
                debugPrint(mapItem.name ?? "")
                self.addPlacemarkToMap(placemark: mapItem.placemark)
            }
        }
    }
    
    private func getRandomRating() -> Double {
        return Double.random(in: 1...5)
    }
    
    // This function is used to add the placemark on the map
    private func addPlacemarkToMap(placemark: CLPlacemark) {
        guard let location = placemark.location, let name = placemark.name else { return }
        
        let business = Business(name: name, location: location, rating: getRandomRating())
        let businessVM = annotationFactory.createBusinessViewModel(business: business)
        
        self.mapView.addAnnotation(businessVM)
    }

}

// MARK: - MKMapViewDelegate

extension MainViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.008, longitudeDelta: 0.008))
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }
        
        var businessAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "BusinessAnnotationView")
        
        if businessAnnotationView == nil, let businessVM = annotation as? BusinessViewModel {
            
            businessAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "BusinessAnnotationView")
            businessAnnotationView?.canShowCallout = true
            businessAnnotationView?.image = businessVM.image
        } else {
            businessAnnotationView?.annotation = annotation
        }
        
        return businessAnnotationView
    }
}

// MARK: - CLLocationManagerDelegate

extension MainViewController: CLLocationManagerDelegate {
    
}

