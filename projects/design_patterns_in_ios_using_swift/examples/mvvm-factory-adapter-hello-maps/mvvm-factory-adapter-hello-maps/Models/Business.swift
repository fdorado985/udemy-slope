//
//  Business.swift
//  mvvm-hello-maps
//
//  Created by Juan Francisco Dorado Torres on 2/24/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import CoreLocation

struct Business {
    
    var name: String
    var location: CLLocation
    var rating: Double
}
