//
//  YelpBusiness.swift
//  mvvm-factory-adapter-hello-maps
//
//  Created by Juan Francisco Dorado Torres on 2/25/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

// This is third party model provided to you by third party

import Foundation
import CoreLocation

struct YelpBusiness: Decodable {
    
    var title: String
    var latitude: Double
    var longitude: Double
    var rating: Double
}

extension YelpBusiness: BusinessProtocol {
    
    func adaptBusinessFromYelp() -> Business {
        let location = CLLocation(latitude: latitude, longitude: longitude)
        return Business(name: self.title, location: location, rating: rating)
    }
}
