//
//  AnnotationFactory.swift
//  mvvm-hello-maps
//
//  Created by Juan Francisco Dorado Torres on 2/24/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import MapKit

class AnnotationFactory {
    
    func createBusinessViewModel(business: Business) -> BusinessViewModel {
        let businessVM = BusinessViewModel(business)
        
        switch business.rating {
        case 4.5...5:
            businessVM.image = UIImage(named: "great")
        case 3...4:
            businessVM.image = UIImage(named: "bad")
        default:
            businessVM.image = UIImage(named: "terrible")
        }
        
        return businessVM
    }
}
