//
//  YelpSearchService.swift
//  mvvm-factory-adapter-hello-maps
//
//  Created by Juan Francisco Dorado Torres on 2/25/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

// This is third party model provided to you by third party

class YelpSearchService: YelpSearchResults {
    
    func getBusinesses(_ completion: @escaping ([YelpBusiness]) -> ()) {
        if let path = Bundle.main.path(forResource: "yelp-businesses", ofType: "json") {
            if let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) {
                guard let results = try? JSONDecoder().decode([YelpBusiness].self, from: jsonData) else {
                    completion([YelpBusiness]())
                    return
                }
                
                completion(results)
            }
        }
    }
}

extension YelpSearchService: SearchResultsProtocol {
    
    func adaptSearchResultsFromYelp(_ completion: @escaping (SearchResults) -> ()) {
        var businesses = [Business]()
        getBusinesses { (yelpBusinesses) in
            for business in yelpBusinesses {
                businesses.append(business.adaptBusinessFromYelp())
            }
            
            completion(SearchResults(businesses: businesses))
        }
    }
}
