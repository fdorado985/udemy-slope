//
//  Protocols.swift
//  mvvm-factory-adapter-hello-maps
//
//  Created by Juan Francisco Dorado Torres on 2/25/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

// This is third party model provided to you by third party

protocol YelpSearchResults {
    
    func getBusinesses(_ completion: @escaping ([YelpBusiness]) -> ())
}

protocol BusinessProtocol {
    
    func adaptBusinessFromYelp() -> Business
}

protocol SearchResultsProtocol {
    
    func adaptSearchResultsFromYelp(_ completion: @escaping (SearchResults) -> ())
}

struct SearchResults {
    
    var businesses: [Business]
}
