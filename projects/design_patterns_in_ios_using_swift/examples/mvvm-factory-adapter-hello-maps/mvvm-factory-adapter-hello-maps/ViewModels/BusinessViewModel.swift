//
//  BusinessViewModel.swift
//  mvvm-hello-maps
//
//  Created by Juan Francisco Dorado Torres on 2/24/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import MapKit

class BusinessViewModel: NSObject, MKAnnotation {
    
    // MARK: Properties
    
    var coordinate: CLLocationCoordinate2D // Inheritance from MKAnnotation
    var title: String?
    var rating: Double
    var image: UIImage?
    
    // MARK: Init
    
    init(coordinate: CLLocationCoordinate2D, title: String, rating: Double) {
        self.coordinate = coordinate
        self.title = title
        self.rating = rating
    }
    
    init(_ business: Business) {
        self.coordinate = business.location.coordinate
        self.title = business.name
        self.rating = business.rating
    }
    
}
