//
//  AppSettingsTableViewController.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class AppSettingsTableViewController: UITableViewController {
    
    // MARK: Properties
    
    let appSettings = AppSettings.shared
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
    }
    
    @IBAction func cancelBarButtonTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true)
    }
}

// MARK: - UITableViewControllerDelegate|UITableViewControllerDataSource

extension AppSettingsTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return QuestionOrderType.allCases.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionOrderTypeTableViewCell", for: indexPath)
        
        let questionOrderType = QuestionOrderType.allCases[indexPath.row]
        cell.textLabel?.text = questionOrderType.title
        cell.textLabel?.textColor = UIColor.white
        cell.accessoryType = appSettings.questionOrderType == questionOrderType ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let questionOrderType = QuestionOrderType.allCases[indexPath.row]
        appSettings.questionOrderType = questionOrderType
        self.tableView.reloadData()
    }
}
