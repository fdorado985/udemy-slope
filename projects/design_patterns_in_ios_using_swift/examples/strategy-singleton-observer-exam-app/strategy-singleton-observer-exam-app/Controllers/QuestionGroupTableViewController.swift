//
//  QuestionGroupTableViewController.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class QuestionGroupTableViewController: UITableViewController {
    
    // MARK: Properties
    
    private var questionGroups = [QuestionGroup]()
    lazy var cellBackgroundView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 100))
        view.backgroundColor = UIColor(displayP3Red: 22 / 255, green: 160 / 255, blue: 133 / 255, alpha: 1.0)
        return view
    }()
    
    // MARK: View Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        self.questionGroups = getQuestionGroups()
    }
    
    // MARK: Functions
    
    private func getQuestionGroups() -> [QuestionGroup] {
        return [
            QuestionGroup(displayName: "Math", course: .math, sourceType: .json),
            QuestionGroup(displayName: "Geography", course: .geography, sourceType: .xml)
        ]
    }
    
    private func setupUI() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
}

// MARK: - TableViewDelegate|TableViewDataSource

extension QuestionGroupTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questionGroups.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionGroupTableViewCell", for: indexPath)
        cell.textLabel?.textColor = UIColor.white
        cell.textLabel?.text = self.questionGroups[indexPath.row].displayName
        cell.selectedBackgroundView = cellBackgroundView
        return cell
    }
}

// MARK: - Navigation

extension QuestionGroupTableViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let indexPath = self.tableView.indexPathForSelectedRow,
            let questionsVC = segue.destination as? QuestionsViewController else { return }
        questionsVC.questionGroup = self.questionGroups[indexPath.row]
    }
}
