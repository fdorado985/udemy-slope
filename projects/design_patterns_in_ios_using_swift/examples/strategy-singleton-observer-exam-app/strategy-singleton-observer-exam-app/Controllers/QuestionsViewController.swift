//
//  QuestionsViewController.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var questionTextLabel: UILabel!
    @IBOutlet weak var questionNumberLabel: UILabel!
    
    // MARK: Properties
    
    var questionGroup: QuestionGroup!
    private var questions = [Question]()
    var questionStrategy: QuestionStrategy!
    let appSettings = AppSettings.shared
    var observable: QuestionStrategyObservable!
    private var keyValueObservation: NSKeyValueObservation?

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.largeTitleDisplayMode = .never
        self.title = questionGroup.displayName
        
        populateQuestions()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.keyValueObservation = nil
    }
    
    // MARK: IBActions
    
    @IBAction func nextQuestionButtonTapped(_ sender: UIButton) {
        showQuestion()
    }
    
    // MARK: Functions
    
    private func populateQuestions() {
        switch questionGroup.sourceType {
        case .json:
            self.questionStrategy = QuestionJSONStrategy(name: questionGroup.course.rawValue)
        case .xml:
            //self.questionStrategy = QuestionXMLStrategy(name: questionGroup.course.rawValue)
            debugPrint("Use XML Strategy")
        }
        
        self.observable = QuestionStrategyObservable(strategy: questionStrategy)
        self.keyValueObservation = self.observable.observe(\.strategy.questionIndex, options: [.new]) { (object, change) in
            self.questionNumberLabel.text = "\(object.strategy.questionIndex)/\(object.strategy.questions.count)"
        }
        showQuestion()
    }
    
    private func showQuestion() {
        if appSettings.questionOrderType == .random {
            questionStrategy.questions.shuffle()
        }
        
        let question = questionStrategy.nextQuestion()
        self.questionTextLabel.text = question.text
    }
}
