//
//  QuestionJSONStrategy.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class QuestionJSONStrategy: NSObject, QuestionStrategy {
    
    // MARK: Properties
    
    var questions: [Question] = []
    dynamic var questionIndex: Int = 0
    var currentQuestion: Question!
    
    // MARK: Init
    
    required init(name: String) {
        if let path = Bundle.main.path(forResource: name, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let questionsDictionary = try JSONDecoder().decode([String : [Question]].self, from: data)
                
                if let qs = questionsDictionary["questions"] {
                    questions = qs
                }
            } catch let err {
                debugPrint("\(#function) something went wrong : \(err.localizedDescription)")
            }
        }
    }
    
    // MARK: Functions
    
    func nextQuestion() -> Question {
        if questionIndex >= questions.count {
            return self.currentQuestion
        }
        
        self.currentQuestion = questions[questionIndex]
        questionIndex += 1
        return self.currentQuestion
    }
}
