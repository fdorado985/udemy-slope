//
//  QuestionStrategy.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

@objc protocol QuestionStrategy {
    
    var questions: [Question] { get set }
    var questionIndex: Int { get set }
    
    init(name: String)
    
    func nextQuestion() -> Question
}
