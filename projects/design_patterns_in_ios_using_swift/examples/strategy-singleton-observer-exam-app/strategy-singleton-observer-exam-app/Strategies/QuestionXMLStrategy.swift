//
//  QuestionXMLStrategy.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class QuestionXMLStrategy: QuestionStrategy {
    
    // MARK: Properties
    
    var questions: [Question] = []
    var questionIndex: Int = 0
    
    // MARK: Init
    
    required init(name: String) {
        // Read an xml file and populate your array
    }
    
    // MARK: Functions
    
    func nextQuestion() -> Question {
        return Question(text: "Dummy question", point: 10, isCorrect: true)
    }
}
