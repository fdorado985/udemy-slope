//
//  QuestionStrategyObservable.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

@objcMembers class QuestionStrategyObservable: NSObject {
    
    // MARK: Properties
    
    dynamic var strategy: QuestionStrategy!
    
    // MARK: Init
    
    init(strategy: QuestionStrategy) {
        self.strategy = strategy
    }
}
