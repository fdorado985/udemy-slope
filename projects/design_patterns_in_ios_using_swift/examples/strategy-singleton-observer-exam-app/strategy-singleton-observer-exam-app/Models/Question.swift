//
//  Question.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Question: NSObject, Decodable {
    
    // MARK: Properties
    
    var text: String
    var point: Double
    var isCorrect: Bool
    
    // MARK: Init
    
    init(text: String, point: Double, isCorrect: Bool) {
        self.text = text
        self.point = point
        self.isCorrect = isCorrect
    }
}
