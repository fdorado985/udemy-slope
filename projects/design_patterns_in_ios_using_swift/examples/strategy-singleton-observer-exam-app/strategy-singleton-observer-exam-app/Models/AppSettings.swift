//
//  AppSettings.swift
//  strategy-singleton-observer-exam-app
//
//  Created by Juan Francisco Dorado Torres on 2/23/19.
//  Copyright © 2019 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class AppSettings {
    
    // MARK: Singleton
    
    static let shared = AppSettings()
    
    // MARK: Properties
    
    private struct Keys {
        static var questionOrderType = "questionOrderType"
    }
    
    var questionOrderType: QuestionOrderType {
        set {
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue.rawValue, forKey: Keys.questionOrderType)
        }
        
        get {
            let userDefaults = UserDefaults.standard
            let rawValue = userDefaults.integer(forKey: Keys.questionOrderType)
            return QuestionOrderType(rawValue: rawValue) ?? QuestionOrderType.sequential
        }
    }
    
    // MARK: Init
    
    private init() { }
}
