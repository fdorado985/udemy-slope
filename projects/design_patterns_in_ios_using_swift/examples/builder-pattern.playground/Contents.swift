import UIKit

// *************************** //
// ***** BUILDER PATTERN ***** //
// *************************** //

struct Hamburger {
    
    let meat: Meat
    let sauce: Sauces
    let toppings: Toppings
}

enum Meat: String {
    
    case beef
    case chicken
    case tofu
}

// OptionSet : It is kind similar to Enum, the only difference is that you can choose multiple options in an OptionSet
struct Sauces: OptionSet {
    
    static let mayonnaise = Sauces(rawValue: 1 << 0) // Bit shift operator
    static let mustard = Sauces(rawValue: 1 << 1)
    static let ketchup = Sauces(rawValue: 1 << 2)
    
    let rawValue: Int
    
    init(rawValue: Int) {
        self.rawValue = rawValue
    }
}


struct Toppings: OptionSet {
    
    static let cheese = Toppings(rawValue: 1 << 0)
    static let lettuce = Toppings(rawValue: 1 << 1)
    static let pickles = Toppings(rawValue: 1 << 2)
    
    let rawValue: Int
    
    init(rawValue: Int) {
        self.rawValue = rawValue
    }
}


/// Responsible to create a Hamburger - This is our Builder
class HamburgerBuilder {
    
    private(set) var meat: Meat = .beef
    private(set) var sauces: Sauces = []
    private(set) var toppings: Toppings = []
    
    func setMeat(_ meat: Meat) {
        self.meat = meat
    }
    
    func addSauces(_ sauce: Sauces) {
        self.sauces.insert(sauce)
    }
    
    func addToppings(_ topping: Toppings) {
        self.toppings.insert(topping)
    }
    
    func removeSauces(_ sauce: Sauces) {
        self.sauces.remove(sauce)
    }
    
    func removeToppings(_ topping: Toppings) {
        self.toppings.remove(topping)
    }
    
    func build() -> Hamburger {
        return Hamburger(meat: meat, sauce: sauces, toppings: toppings)
    }
}


/// This employee is in charge to create the hamburger using the diagram this is the Director
class Employee {
    
    func createCombo() -> Hamburger {
        let builder = HamburgerBuilder()
        builder.setMeat(.beef)
        builder.addSauces([.ketchup, .mayonnaise, .mustard])
        builder.addToppings([.cheese, .lettuce, .pickles])
        return builder.build()
    }
}

// ******************* //
// ***** EXAMPLE ***** //
// ******************* //

let employee = Employee()

let combo = employee.createCombo()
combo.meat
combo.sauce
combo.toppings
