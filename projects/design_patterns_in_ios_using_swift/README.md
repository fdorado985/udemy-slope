# Design Patterns In iOS Using Swift

## Table Of Content
1. [Model-View-Controller](#model-view-controller)
2. [Delegation](#delegation)
3. [Strategy](#strategy)
4. [Singleton](#singleton)
5. [Observer](#observer)
6. [Builder](#builder)
7. [Model-View-ViewModel](#model-view-viewmodel)
8. [Factory](#factory)
9. [Adapter](#adapter)
10. [Iterator](#iterator)

## Model View Controller
It is the default pattern to create any iOS application.

The MVC pattern concerns on three different types, the **Model**, the **View** and the **Controller**

![mvc](.screenshots/mvc.png)

* **View Controller** : That is the middle man, the conductor, the person.
* **The Model** : It is the one who holds the data, so whatever data you want to display on screen, the model is holding it.
* **View** : It can be anything that is basically use to display the data.

The **View** can talk to the **View Controller** using the *IBActions* and the **Model** can talk to the **View Controller** with *Property Observing*

## Delegation
Enables an object to use another helper object, to provide data or perform a task rather than the task itself.

![delegate](.screenshots/delegate.png)

* **Object Needing a Delegate** : Known as delegating object, this is the object that has the delgate.
* **Delegate Protocol** : It is the one that defines what is going to be shared, or what do you want to implement.
* **Object Acting as a Delegate** : This can be the actual object that is needing that particular information from the delegate protocol itself.

It is a very important pattern.

## Strategy
Define the interchangeable objects that can be set or switch during run time, it has three parts.

![strategy](.screenshots/strategy.png)

* **Object Using A Strategy** : Most of the cases this must be the ViewController... it could not but it is most of the times.
* **Protocol Strategy** : These are the properties, the functions that have to be implemented on a concrete implementation (concrete strategy).
* **Concrete Strategy*** : Also known as concrete implementation, all these implementations must conform the **Protocol Strategy**.

The way it works is that any concrete strategy, which is conforming the protocol strategy can be passed as a part of the chosen strategy.

## Singleton
This pattern restricts a class to be only one instance.
So every time you referred to the class the same instance is going to be returned.

![singleton](.screenshots/singleton.png)

The Singleton pattern we create this pattern, creating a static property, or a static field, that will be able to return us the same instance of that class.

Doing in this way you are not gonna be able to create another instance of that Singleton

> There are other patterns like **Singleton+** that allows you to create another instance of the singleton

## Observer
This lets one object observes changes in another object.
It consists on different types of objects.

![observer](.screenshots/observer.png)

* **Subject** : It is the object that is being observed.
* **Observer** : This is the object doing the observing.

> At this time Swift doesn't have any language support for the key value *Observing*, but we can use some Objective-C features to do that.

* **Observable** : This will launch a notification to take advice about the object has changed.

## Builder
This allows the creation of complex objects step-by-step instead of all at once.
This is done through the initializer and the pattern involves three main different types.

![builder](.screenshots/builder.png)

* **Director** : This is the one who is using the *Builder* pattern, this could be our *ViewController* but also it can be something else, but the most common is the *ViewController*
* **Builder** : It is the responsible to create a complicated product, all the complexity is contained just right here.
* **Product** : This is the final complicated object created with the Builder.

## Model-View-ViewModel
This is one of the most popular architecture pattern for the iOS applications.

![mvvm](.screenshots/mvvm.png)

The real escence is that the **View Controller** creates or **owns** the **View Models**.

The **View Model** is a **Model** that is for the **View**.

Example... If you want to display some sort of information asking the user to change the password, the **View Model** consists of the *Password* and the *New Password*.

That particular **Model** will configure the **View**.

The main purpose is to direct what is displayed on the View.
Is the View is very complex... then it is responsability of the View Model.

## Factory
It provides the way to create objects without exposing the creation logic, and we here on our diagram just have two different types.

![factory](.screenshots/factory.png)

The basic point of **Factory** is hide all the logic and complexity to create an object.

## Adapter
Allows incompatible types to work together and it involves 2 different components.

![adapter](.screenshots/adapter.png)

* **Object using an adapter** : It is the object that depends on the new protocol
* **Protocol - New Protocol** : It is the protocol that you as developer are going to write and it is going to allow you to communicate with the Legacy Object.
* **Adapter** : This is the connection between the protocol and the Legacy Object.
* **Legacy Object** : It could be a third party service, or another framework... that cannot be modified.

## Iterator
Provides a standard way to loop through a collection, and this pattern involves two different types.

![iterator](.screenshots/iterator.png)

* **<< Protocol >> - IteratorProtocol** : It is the iterator protocol which makes your iterator object interpretable
* **Iterator Object** : This is the object you want to make it iteratable.
