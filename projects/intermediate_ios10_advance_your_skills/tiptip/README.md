#  Tip Tip

## Description
On this app you will be able to see how to use `Localization` to make your app multilanguage.
There are two ways... and you will see.. both on this app... one using the Localization storyboard and using `Localizable.string` file.

What is about the app... well it is a tip calculator.

## Demo
### English
![tip_tip_english_demo](screenshots/tip_tip_english_demo.gif)

### Spanish
![tip_tip_spanish_demo](screenshots/tip_tip_spanish_demo.gif)
