//
//  MainVC.swift
//  tiptip
//
//  Created by Juan Francisco Dorado Torres on 8/12/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var txtBill: UITextField!
    @IBOutlet weak var txtTip: UITextField!
    @IBOutlet weak var txtTotal: UITextField!
    @IBOutlet weak var segTipPercentage: UISegmentedControl!
    @IBOutlet weak var lblTip: UILabel!
    
    // MARK: Properties
    
    private var tipPercentage: Double {
        switch segTipPercentage.selectedSegmentIndex {
        case 0:
            return 0.10
        case 1:
            return 0.15
        case 2:
            return 0.20
        default:
            return 0.10
        }
    }
    
    private var currencyFormatter: NumberFormatter {
        let _currencyFormatter = NumberFormatter()
        _currencyFormatter.numberStyle = .currency
        return _currencyFormatter
    }
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Using Localizable.string
        lblTip.text = NSLocalizedString("mainvc_tip_label", comment: "")
    }
    
    // MARK: IBActions
    
    @IBAction func txtBillChanged(_ sender: UITextField) {
        calculateTip()
    }
    
    @IBAction func tipPercentageValueChanged(_ sender: UISegmentedControl) {
        txtBill.resignFirstResponder()
        calculateTip()
    }
    
    // MARK: Functions
    
    private func calculateTip() {
        let billText = txtBill.text ?? ""
        guard let bill = (billText == "") ? 0.00 : Double(billText) else { return }
        
        let tip = tipPercentage * bill
        let total = bill + tip
        
        txtTip.text = currencyFormatter.string(from: NSNumber(value: tip))
        txtTotal.text = currencyFormatter.string(from: NSNumber(value: total))
    }
}

