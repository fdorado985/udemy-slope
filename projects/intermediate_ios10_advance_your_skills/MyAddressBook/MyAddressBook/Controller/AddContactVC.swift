//
//  AddContactVC.swift
//  MyAddressBook
//
//  Created by Juan Francisco Dorado Torres on 5/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

// REAL PATH: Realm.Configuration.defaultConfiguration.fileURL
// CMD + Shift + G

import UIKit
import RealmSwift

class AddContactVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var txtName: UITextField!
  @IBOutlet weak var txtPhone: UITextField!
  @IBOutlet weak var txtEmail: UITextField!
  @IBOutlet weak var imgContact: UIImageView!

  // MARK: - Properties

  private var imageController: UIImagePickerController?
  var contact: Contact?

  // MARK: - Enums

  private enum SaveType {
    case new
    case current
  }

  // MARK: - ViewCycle

  override func viewDidLoad() {
    super.viewDidLoad()

    imageController = UIImagePickerController()
    verifyForUpdateContact()
  }

  // MARK: - IBActions

  @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
    let saveType = contact == nil ? SaveType.new : SaveType.current
    addContact(for: saveType)
  }
  
  @IBAction func addImageTapped(_ sender: UITapGestureRecognizer) {
    guard let imageController = imageController else { return }
    imageController.sourceType = .photoLibrary
    imageController.delegate = self
    present(imageController, animated: true, completion: nil)
  }

  // MARK: - Functions

  private func verifyForUpdateContact() {
    if let contact = contact {
      self.title = "Update Contact"
      txtName.text = contact.name
      txtPhone.text = contact.phone
      txtEmail.text = contact.email

      if let imageData = contact.imgData as Data? {
        let image = UIImage(data: imageData)
        imgContact.image = image
      }
    }
  }

  private func addContact(for type: SaveType) {
    let realm = try! Realm()
    switch type {
    case .new:
      let contact = Contact()
      contact.name = txtName.text ?? ""
      contact.phone = txtPhone.text ?? ""
      contact.email = txtEmail.text ?? ""
      if let image = imgContact.image,
        let imgData = UIImageJPEGRepresentation(image, 1.0) as NSData? {
        contact.imgData = imgData
      }

      try? realm.write {
        realm.add(contact)
      }
    case .current:
      try? realm.write {
        contact?.name = txtName.text ?? ""
        contact?.phone = txtPhone.text ?? ""
        contact?.email = txtEmail.text ?? ""
        if let image = imgContact.image,
          let imgData = UIImageJPEGRepresentation(image, 1.0) as NSData? {
          contact?.imgData = imgData
        }
      }
    }
    navigationController?.popViewController(animated: true)
  }
}

// MARK: - UIImagePickerControllerDelegate

extension AddContactVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
    imgContact.image = selectedImage
    picker.dismiss(animated: true, completion: nil)
  }
}
