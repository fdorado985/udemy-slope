//
//  MainVC.swift
//  MyAddressBook
//
//  Created by Juan Francisco Dorado Torres on 5/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import RealmSwift

class MainVC: UIViewController {

  // MARK: - IBOutlets

  @IBOutlet weak var tableContacts: UITableView!

  // MARK: - Properties

  var contacts: Results<Contact>?

  // MARK: - ViewCycle

  override func viewDidLoad() {
    super.viewDidLoad()

    let realm = try! Realm()
    contacts = realm.objects(Contact.self)
      .sorted(byKeyPath: "name", ascending: true)
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    tableContacts.reloadData()
  }
}

// MARK: - UITableViewDelegate|UITableViewDataSource

extension MainVC: UITableViewDelegate, UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return contacts?.count ?? 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()

    let contact = contacts?[indexPath.row]
    cell.textLabel?.text = contact?.name
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let contact = contacts?[indexPath.row]
    performSegue(withIdentifier: "addcontact", sender: contact)
  }
}

// MARK: - Prepare For Segue

extension MainVC {

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "addcontact",
      let contact = sender as? Contact,
      let addContactVC = segue.destination as? AddContactVC {
      addContactVC.contact = contact
    }
  }
}
