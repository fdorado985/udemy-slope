//
//  Contact.swift
//  MyAddressBook
//
//  Created by Juan Francisco Dorado Torres on 5/25/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import RealmSwift

class Contact: Object {

  @objc dynamic var name = ""
  @objc dynamic var phone = ""
  @objc dynamic var email = ""
  @objc dynamic var imgData: NSData?
}
