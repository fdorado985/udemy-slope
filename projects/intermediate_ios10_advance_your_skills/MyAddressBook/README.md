#  My Address Book

On this app we use [Realm](https://realm.io) as database, so you can see a simple and easy way to save and update values on a database and avoid those headaches of Core Data!

## Demo
![Demo](screenshots/my-address-book-demo.gif)
