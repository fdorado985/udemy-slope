// Map - Filter - Reduce

let luckyNumbers = [1, 2, 3, 4, 5]
let dogs = ["Fido" : 8, "Sarah" : 32, "Sean" : 16]
let myTuple: (String, Int) = ("Hello", 3)

// Map - Allows you to transform whatever you have in an array to another thing (in general)

// Example working with an array
let newLuckyNumbers = luckyNumbers.map { $0 * 2 }
let stringLuckyNumbers = luckyNumbers.map { String($0) }

print(newLuckyNumbers)
print(stringLuckyNumbers)

// Example working with dictionaries
let newDogMap = dogs.map { "\($0) is \($1)" }

print(newDogMap)


// Filter - Will give you the elements that have a given condition

// Example working with an array
let oddNumbers = luckyNumbers.filter { $0 % 2 == 0 }
print(oddNumbers)

//Example working with dictionaries
let olderDogs = dogs.filter{ $1 > 10 }
print(olderDogs)


// Reduce - As the names says... it reduces a bunch of values in one value

// Example working with an array
luckyNumbers.reduce(0) { (result, value) -> Int in
  return result + value
}

luckyNumbers.reduce("") { (result, value) -> String in
  return result + String(value)
}

luckyNumbers.reduce(0, { $0 + $1 })

// Example working with dictionaries
dogs.reduce(0) { (result, theDog) -> Int in
  return result + theDog.value
}

dogs.reduce("") { (result, theDog) -> String in
  return result + String(theDog.value)
}

dogs.reduce(0, { $0 + $1.value })

dogs.reduce("", { $0 + String($1.key) })
