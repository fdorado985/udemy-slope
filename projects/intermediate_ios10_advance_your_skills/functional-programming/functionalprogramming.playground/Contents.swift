// What is functional programming ========

// 1. Avoid State

// 2. Pure Functions

func addNumber(num1: Int, num2: Int) -> Int {
  return num1 + num2
}

addNumber(num1: 4, num2: 5)


// Clousures ============

var addTwoNums: (Int, Int) -> Int

addTwoNums = { (num1, num2) in
  return num1 + num2
}

addTwoNums(3, 4)


func adding(num1: Int, num2: Int) -> Int {
  return num1 + num2
}

addTwoNums = adding(num1:num2:)


func doMath(num1: Int, num2: Int, mathFunc: (Int, Int) -> Int) -> Int {
  return mathFunc(num1, num2)
}

func multiply(num1: Int, num2: Int) -> Int {
  return num1 * num2
}

doMath(num1: 3, num2: 5, mathFunc: multiply(num1:num2:))
doMath(num1: 4, num2: 6) { (num1, num2) -> Int in
  return num1 * num2
}


// Higher Order Functions and Typealias ===========

let myStrings = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "k", "l", "m", "n"]

func upperCaseArray(strings: [String]) -> [String] {
  var newArray = [String]()
  for value in strings {
    newArray.append(value.uppercased())
  }
  return newArray
}

func doubleArray(strings: [String]) -> [String] {
  var newArray = [String]()
  for value in strings {
    newArray.append(value + value)
  }
  return newArray
}

upperCaseArray(strings: myStrings)
doubleArray(strings: myStrings)


func changeArray(strings: [String], theEditFunction: (String) -> String) -> [String] {
  var newArray = [String]()
  for value in strings {
    newArray.append(theEditFunction(value))
  }
  return newArray
}

changeArray(strings: myStrings) { (value) -> String in
  return value.uppercased()
}

func newUpperCaseArray(strings: [String]) -> [String] {
  return changeArray(strings: strings, theEditFunction: { $0.uppercased() })
}

newUpperCaseArray(strings: myStrings)
