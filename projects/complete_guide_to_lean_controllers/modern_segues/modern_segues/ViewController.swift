//
//  ViewController.swift
//  better_segues
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController, SegueHandler {
    
    // MARK: - SegueHandler Properties
    
    enum SegueIdentifier: String {
        case addItem = "addItemsSegue"
        case showItems = "showItemsSegue"
    }
    
    // MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(for: segue) {
        case .addItem:
            print("addItems")
        case .showItems:
            print("showItems")
        }
    }
}

