//
//  ViewController.swift
//  better_segues
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: Properties
    
    private let ADD_VIEW_CONTROLLERS_SEGUE = "AddVC"
    private let SHOW_ITEMS_VIEW_CONTROLLER_SEGUE = "ShowItemsVC"
    
    // MARK: Initialization

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // MARK: - Functions
    
    func performSegueForAddViewController(segue: UIStoryboardSegue) {
        
    }
    
    func performSegueForShowItemsViewController(segue: UIStoryboardSegue) {
        
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ADD_VIEW_CONTROLLERS_SEGUE {
            // Do something
            // Task1
            // Task2
            // Taks3
            performSegueForAddViewController(segue: segue)
        } else if segue.identifier == SHOW_ITEMS_VIEW_CONTROLLER_SEGUE {
            // Do another stuff
            // Task1
            // Task2
            // Taks3
            performSegueForShowItemsViewController(segue: segue)
        }
    }
}

