# Complete Guide to Lean Controllers in iOS

## What is a Massive View Controller
A massive `View Controller`:
* **DOES NOT FOLLOW** Single Responsibility Principle
* **DOES NOT FOLLOW** Composition
* **DOES NOT PROVIDE** Reusability

### Example
* Setting up database
* Creating User Controls
* Fetching records from persistent storage

## What is a Lean View Controller

This is not about reduce the number of lines... it is more about composition, it is more about put the components in the right place... in the right order.

![massive_view_controller](assets/massive_view_controller.png)

It handles different tasks... like in the image above... configure database, create user controls, fetch data... and you are not able to reuse them, all of this tasks are inside of the view controller, if you want to reuse, you will need to create another `view controller` use it as a `massive view controller` and start reusing what is already in this `massive view controller`.

![lean_view_controller](assets/lean_view_controller.png)

On a lean view controller are this tasks... become components reusable.

Again... it is not about reduce lines of code... it is about put the things in the correct place!
