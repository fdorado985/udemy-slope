//
//  ShoppingListVC.swift
//  grocery
//
//  Created by Juan Francisco Dorado Torres on 7/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreData

class ShoppingListVC: UITableViewController, UITextFieldDelegate, NSFetchedResultsControllerDelegate {
    
    // MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext!
    var fetchResultsController: NSFetchedResultsController<ShoppingList>!

    // MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeCoreDataStack()
        populateShoppingList()
    }
    
    // MARK: - Functions
    
    func initializeCoreDataStack() {
        guard let modelUrl = Bundle.main.url(forResource: "grocery", withExtension: "momd") else {
            fatalError("GroceryDataModel not found")
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl) else {
            fatalError("Unable to initialize ManagedObjectModel")
        }
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let fileManager = FileManager()
        
        guard let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            fatalError("Unable to get documents URL")
        }
        
        let storeUrl = documentsUrl.appendingPathComponent("grocery.sqlite")
        print(storeUrl)
        
        try! persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: nil)
        let type = NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: type)
        self.managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
    }
    
    private func populateShoppingList() {
        let request = NSFetchRequest<ShoppingList>(entityName: "ShoppingList")
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        self.fetchResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        self.fetchResultsController.delegate = self
        
        do {
            try self.fetchResultsController.performFetch()
        } catch {
            fatalError("Cannot be able to fetch from FetchResultsController")
        }
    }
    
    // MARK: - UITableView
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 44))
        headerView.backgroundColor = .lightText
        
        let textField = UITextField(frame: headerView.frame)
        textField.placeholder = "Enter shopping list"
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        textField.leftViewMode = .always
        textField.delegate = self
        headerView.addSubview(textField)
        
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.fetchResultsController.sections else {
            return 0
        }
        
        return sections[section].numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingListCell", for: indexPath)
        let shoppingList = self.fetchResultsController.object(at: indexPath)
        cell.textLabel?.text = shoppingList.title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let shoppingList = self.fetchResultsController.object(at: indexPath)
            self.managedObjectContext.delete(shoppingList)
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Cannot able to save the current changes")
            }
        }
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let shoppingList = NSEntityDescription.insertNewObject(forEntityName: "ShoppingList", into: self.managedObjectContext) as? ShoppingList else {
            fatalError("Cannot cast to Shopping List")
        }
        shoppingList.title = textField.text
        
        do {
            try self.managedObjectContext.save()
        } catch {
            fatalError("Cannot be able to save")
        }
        
        return textField.resignFirstResponder()
    }
    
    // MARK: - NSFetchedResultsController
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { fatalError("NewIndexPath is nil") }
            self.tableView.insertRows(at: [newIndexPath], with: .automatic)
        case .delete:
            guard let indexPath = indexPath else { fatalError("NewIndexPath is nil") }
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        default:
            break
        }
        
    }

}
