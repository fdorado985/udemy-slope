//
//  MyBillsVC.swift
//  secure_tabs
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MyBillsVC: UIViewController {

    // MARK: Properties
    
    private let isLoggedIn = false
    
    // MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !isLoggedIn {
            guard let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {
                fatalError("LoginVC is not found")
            }
            
            self.present(loginVC, animated: true)
        }
    }

}
