//
//  BaseTabBarController.swift
//  secure_tabs
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {  }

extension BaseTabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let controller = viewController as? LoginHandler else { return true }
        let isValid = controller.validate()
        if !isValid {
            guard let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {
                fatalError("LoginVC was not found")
            }
            self.present(loginVC, animated: true)
        }
        
        return isValid
    }
}
