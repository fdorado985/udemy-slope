//
//  LoginHandler.swift
//  secure_tabs
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

protocol LoginHandler: class {
    
    func validate() -> Bool
}

extension LoginHandler where Self: UIViewController {
    
    func validate() -> Bool {
        return false
    }
}
