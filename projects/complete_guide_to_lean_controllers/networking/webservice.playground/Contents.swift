//: Playground - noun: a place where people can play

import UIKit
import PlaygroundSupport

typealias JSONDictionary = [String : Any]

struct Pokemon {
    var name: String
}

extension Pokemon {
    init?(dictionary: [String : Any]) {
        guard let name = dictionary["name"] as? String else { return nil }
        self.name = name
    }
    
    static var all = Resource<[Pokemon]>(url: url) { (data) -> [Pokemon]? in
        let json = try? JSONSerialization.jsonObject(with: data!, options: [])
        let dictionaries = json as! JSONDictionary
        let results = dictionaries["results"] as? [JSONDictionary]
        return results?.compactMap(Pokemon.init)
    }
}


//let url = URL(string: "http://pokeapi.co/api/v2/pokemon/")!
//URLSession.shared.dataTask(with: url) { (data, response, error) in
//    do {
//        let json = try JSONSerialization.jsonObject(with: data!, options: [])
//        let dictionaries = json as! [[String : Any]]
//        dictionaries.compactMap(Pokemon.init)
//    } catch {
//        fatalError("Unable to get json \(error)")
//    }
//}

struct Resource<T> {
    let url: URL
    let parse: (Data?) -> T?
}

class Webservice {
    func load<T>(_ resource: Resource<T>, handler: @escaping (T?) -> ()) {
        URLSession.shared.dataTask(with: resource.url) { (data, _, _) in
            DispatchQueue.main.async {
                handler(resource.parse(data))
            }
        }.resume()
    }
}


let url = URL(string: "http://pokeapi.co/api/v2/pokemon/")!
//let resource = Resource<[Pokemon]>(url: url) { (data) -> [Pokemon]? in
//    let json = try? JSONSerialization.jsonObject(with: data!, options: [])
//    let dictionaries = json as! JSONDictionary
//    let results = dictionaries["results"] as? [JSONDictionary]
//    return results?.compactMap(Pokemon.init)
//}

Webservice().load(Pokemon.all) { (pokemons) in
    if let pokemons = pokemons {
        print(pokemons)
    }
}


PlaygroundPage.current.needsIndefiniteExecution = true
