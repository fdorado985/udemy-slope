//
//  CoreDataManager.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    
    // MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext!
    
    // MARK: - Functions
    
    func initializeCoreDataStack() {
        guard let modelUrl = Bundle.main.url(forResource: "grocery_lean_controller", withExtension: "momd") else {
            fatalError("GroceryDataModel not found")
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl) else {
            fatalError("Unable to initialize ManagedObjectModel")
        }
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let fileManager = FileManager()
        
        guard let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            fatalError("Unable to get documents URL")
        }
        
        let storeUrl = documentsUrl.appendingPathComponent("grocery_lean_controller.sqlite")
        print(storeUrl)
        
        try! persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: nil)
        let type = NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: type)
        self.managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
    }
}
