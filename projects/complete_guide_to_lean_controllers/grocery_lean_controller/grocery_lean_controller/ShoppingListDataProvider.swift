//
//  ShoppingListDataProvider.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import CoreData

/*protocol ShoppingListDataProviderDelegate: class {
    func shoppingListDataProviderDidInsert(at newIndexPath: IndexPath)
    func shoppingListDataProviderDidDelete(at indexPath: IndexPath)
}

class ShoppingListDataProvider: NSObject, NSFetchedResultsControllerDelegate {
    
    // MARK: - Properties
    
    weak var delegate: ShoppingListDataProviderDelegate?
    var managedObjectContext: NSManagedObjectContext
    var fetchedResultsController: NSFetchedResultsController<ShoppingList>
    var sections: [NSFetchedResultsSectionInfo]? {
        get {
            return self.fetchedResultsController.sections
        }
    }
    
    // MARK: - Initialization
    
    init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
        let request = NSFetchRequest<ShoppingList>(entityName: "ShoppingList")
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        super.init()
        
        self.fetchedResultsController.delegate = self
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            fatalError("Cannot be able to perform results")
        }
    }
    
    // MARK: - Functions
    
    func object(at indexPath: IndexPath) -> ShoppingList {
        return self.fetchedResultsController.object(at: indexPath)
    }
    
    func delete(_ shoppingList: ShoppingList) {
        self.managedObjectContext.delete(shoppingList)
        do {
            try self.managedObjectContext.save()
        } catch {
            fatalError("Cannot able to save the current changes")
        }
    }
    
    // MARK: - NSFetchedResultsController
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { fatalError("Could not get the newIndexPath") }
            self.delegate?.shoppingListDataProviderDidInsert(at: newIndexPath)
        case .delete:
            guard let indexPath = indexPath else { fatalError("Could not get the indexPath") }
            self.delegate?.shoppingListDataProviderDidDelete(at: indexPath)
        default:
            fatalError("It wasn't Insert neither Delete")
        }
    }
}*/
