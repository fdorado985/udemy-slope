//
//  ManagedObjectType.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

protocol ManagedObjectType: class {
    
    static var entityName: String { get }
}
