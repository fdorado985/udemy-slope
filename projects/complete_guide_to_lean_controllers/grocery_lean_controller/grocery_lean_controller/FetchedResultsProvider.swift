//
//  FetchedResultsProvider.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import CoreData

protocol FetchedResultsProviderDelegate: class {
    func fetchedResultsProviderDidInsert(at newIndexPath: IndexPath)
    func fetchedResultsProviderDidDelete(at indexPath: IndexPath)
}

class FetchedResultsProvider<T: NSManagedObject> : NSObject, NSFetchedResultsControllerDelegate where T: ManagedObjectType {
    
    // MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext
    var fetchedResultsController: NSFetchedResultsController<T>
    weak var delegate: FetchedResultsProviderDelegate?
    
    // MARK: - Initialization
    
    init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
        
        let request = NSFetchRequest<T>(entityName: T.entityName)
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        super.init()
        self.fetchedResultsController.delegate = self
        
        do {
            try self.fetchedResultsController.performFetch()
        } catch {
            fatalError("Unable to fetch results")
        }
    }
    
    // MARK: - Functions
    
    func numberOfSections() -> Int {
        guard let sections = self.fetchedResultsController.sections else { return 0 }
        return sections.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        guard let sections = self.fetchedResultsController.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func object(at index: IndexPath) -> T {
        return self.fetchedResultsController.object(at: index)
    }
    
    func delete(_ shoppingList: T) {
        self.managedObjectContext.delete(shoppingList)
        do {
            try self.managedObjectContext.save()
        } catch {
            fatalError("Cannot able to save the current changes")
        }
    }
    
    // MARK: - NSFetchedResultsController Delegate
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { fatalError("Could not get the newIndexPath") }
            self.delegate?.fetchedResultsProviderDidInsert(at: newIndexPath)
        case .delete:
            guard let indexPath = indexPath else { fatalError("Could not get the indexPath") }
            self.delegate?.fetchedResultsProviderDidDelete(at: indexPath)
        default:
            fatalError("It wasn't Insert neither Delete")
        }
    }
}
