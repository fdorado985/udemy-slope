//
//  ShoppingListVC.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreData

// MARK: - Using complete Generics
class ShoppingListVC: UITableViewController, AddNewItemDelegate {
    
    // MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext!
    var fetchedResultsProvider: FetchedResultsProvider<ShoppingList>!
    var dataSource: TableViewDataSource<UITableViewCell, ShoppingList>!
    
    // MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateShoppingList()
    }
    
    // MARK: - Functions
    
    private func populateShoppingList() {
        self.fetchedResultsProvider = FetchedResultsProvider(managedObjectContext: self.managedObjectContext)
        self.dataSource = TableViewDataSource(cellIdentifier: "ShoppingListCell", tableView: self.tableView, fetchedResultsProvider: self.fetchedResultsProvider) { cell, model in
            cell.textLabel?.text = model.title
            // Other stuffs for your cell
        }
        self.tableView.dataSource = self.dataSource
    }
    
    private func addNewShoppingList(title: String) {
        guard let shoppingList = NSEntityDescription.insertNewObject(forEntityName: "ShoppingList", into: self.managedObjectContext) as? ShoppingList else {
            fatalError("Cannot cast to Shopping List")
        }
        shoppingList.title = title
        
        do {
            try self.managedObjectContext.save()
        } catch {
            fatalError("Cannot be able to save")
        }
    }
    
    // MARK: - UITableView Delegate
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let addNewItemView = AddNewItemView(controller: self, placeHolderText: "Enter new shopping list...")
        addNewItemView.delegate = self
        return addNewItemView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    // MARK: - AddNewItemView Delegate
    
    func addNewItemViewDidAddNewText(text: String) {
        self.addNewShoppingList(title: text)
    }
}


// MARK: - Using Delegates
// ==========================================================
// ========= ADD NEW ITEM VIEW USING DELEGATES ==============
// ==========================================================
/*class ShoppingListVC: UITableViewController, AddNewItemDelegate {
    
    // MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext!
    var shoppingListDataProvider: ShoppingListDataProvider! // This gives you the items
    var shoppingListDataSource: ShoppingListDataSource! // This includes the functionality of datasource
    
    // MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateShoppingList()
    }
    
    // MARK: - Functions
    
    private func populateShoppingList() {
        self.shoppingListDataProvider = ShoppingListDataProvider(managedObjectContext: self.managedObjectContext)
        self.shoppingListDataSource = ShoppingListDataSource(cellIdentifier: "ShoppingListCell", tableView: self.tableView, shoppingListDataProvider: self.shoppingListDataProvider)
        self.tableView.dataSource = self.shoppingListDataSource

    }
    
    private func addNewShoppingList(title: String) {
        guard let shoppingList = NSEntityDescription.insertNewObject(forEntityName: "ShoppingList", into: self.managedObjectContext) as? ShoppingList else {
            fatalError("Cannot cast to Shopping List")
        }
        shoppingList.title = title
        
        do {
            try self.managedObjectContext.save()
        } catch {
            fatalError("Cannot be able to save")
        }
    }
    
    // MARK: - UITableView Delegate
    
    // ==========================================================
    // ========= ADD NEW ITEM VIEW USING DELEGATES ==============
    // ==========================================================
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
     let addNewItemView = AddNewItemView(controller: self, placeHolderText: "Enter new shopping list...")
     addNewItemView.delegate = self
     return addNewItemView
     }
    
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    // MARK: - AddNewItemView Delegate
    
    // ==========================================================
    // ========= ADD NEW ITEM VIEW USING DELEGATES ==============
    // ==========================================================
    func addNewItemViewDidAddNewText(text: String) {
     self.addNewShoppingList(title: text)
     }
}*/

// MARK: - Using Closures
// ==========================================================
// ========= ADD NEW ITEM VIEW USING CLOUSURES ==============
// ==========================================================
/*class ShoppingListVC: UITableViewController {
    
    // MARK: - Properties
    
    var managedObjectContext: NSManagedObjectContext!
    var shoppingListDataProvider: ShoppingListDataProvider! // This gives you the items
    var shoppingListDataSource: ShoppingListDataSource! // This includes the functionality of datasource
    
    // MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateShoppingList()
    }
    
    // MARK: - Functions
    
    private func populateShoppingList() {
        self.shoppingListDataProvider = ShoppingListDataProvider(managedObjectContext: self.managedObjectContext)
        self.shoppingListDataSource = ShoppingListDataSource(cellIdentifier: "ShoppingListCell", tableView: self.tableView, shoppingListDataProvider: self.shoppingListDataProvider)
        self.tableView.dataSource = self.shoppingListDataSource
    }
    
    private func addNewShoppingList(title: String) {
        guard let shoppingList = NSEntityDescription.insertNewObject(forEntityName: "ShoppingList", into: self.managedObjectContext) as? ShoppingList else {
            fatalError("Cannot cast to Shopping List")
        }
        shoppingList.title = title
        
        do {
            try self.managedObjectContext.save()
        } catch {
            fatalError("Cannot be able to save")
        }
    }
    
    // MARK: - UITableView Delegate
    
    // ==========================================================
    // ========= ADD NEW ITEM VIEW USING CLOUSURES ==============
    // ==========================================================
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let addNewItemView = AddNewItemView(controller: self, placeHolderText: "Enter new shopping list...") { text in
            self.addNewShoppingList(title: text)
        }
        return addNewItemView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
}*/

