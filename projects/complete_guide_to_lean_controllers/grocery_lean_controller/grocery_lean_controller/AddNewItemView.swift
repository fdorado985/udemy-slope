//
//  AddNewItemView.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

// ==========================================================
// ========= ADD NEW ITEM VIEW USING DELEGATES ==============
// ==========================================================

protocol AddNewItemDelegate: class {
    func addNewItemViewDidAddNewText(text: String)
}

class AddNewItemView: UIView, UITextFieldDelegate {
    
    // MARK: - Properties
    
    var placeHolder: String
    weak var delegate: AddNewItemDelegate?
    
    // MARK: - Initialization
    
    init(controller: UIViewController, placeHolderText: String) {
        self.placeHolder = placeHolderText
        super.init(frame: controller.view.frame)
        setup()
    }
    
    override init(frame: CGRect) {
        self.placeHolder = ""
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Functions
    
    private func setup() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 44))
        headerView.backgroundColor = .lightText
        
        let textField = UITextField(frame: headerView.frame)
        textField.placeholder = placeHolder
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        textField.leftViewMode = .always
        textField.delegate = self
        
        headerView.addSubview(textField)
        self.addSubview(headerView)
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let newText = textField.text, !newText.isEmpty else { return textField.resignFirstResponder() }
        textField.text = ""
        delegate?.addNewItemViewDidAddNewText(text: newText)
        return textField.resignFirstResponder()
    }
}


// ==========================================================
// ========= ADD NEW ITEM VIEW USING CLOUSURES ==============
// ==========================================================

/*class AddNewItemView: UIView, UITextFieldDelegate {
    
    // MARK: - Properties
    
    var placeHolder: String
    var addNewItemViewClosure: ((String) -> ())?
    
    // MARK: - Initialization
    
    init(controller: UIViewController, placeHolderText: String, addNewItemViewClosure: @escaping (String) -> ()) {
        self.placeHolder = placeHolderText
        self.addNewItemViewClosure = addNewItemViewClosure
        super.init(frame: controller.view.frame)
        setup()
    }
    
    override init(frame: CGRect) {
        self.placeHolder = ""
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Functions
    
    private func setup() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 44))
        headerView.backgroundColor = .lightText
        
        let textField = UITextField(frame: headerView.frame)
        textField.placeholder = placeHolder
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
        textField.leftViewMode = .always
        textField.delegate = self
        
        headerView.addSubview(textField)
        self.addSubview(headerView)
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let newText = textField.text, !newText.isEmpty else { return textField.resignFirstResponder() }
        textField.text = ""
        self.addNewItemViewClosure?(newText)
        return textField.resignFirstResponder()
    }
}*/
