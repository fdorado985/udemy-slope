//
//  ShoppingListDataSource.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/30/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

/*class ShoppingListDataSource: NSObject, UITableViewDataSource, ShoppingListDataProviderDelegate {
    
    // MARK: - Properties
    
    var cellIdentifier: String
    var tableView: UITableView
    var shoppingListDataProvider: ShoppingListDataProvider
    
    init(cellIdentifier: String, tableView: UITableView, shoppingListDataProvider: ShoppingListDataProvider) {
        self.cellIdentifier = cellIdentifier
        self.tableView = tableView
        self.shoppingListDataProvider = shoppingListDataProvider
        
        super.init()
        self.shoppingListDataProvider.delegate = self
    }
    
    // MARK: - UITableView DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.shoppingListDataProvider.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
        let shoppingList = self.shoppingListDataProvider.object(at: indexPath)
        cell.textLabel?.text = shoppingList.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let shoppingList = self.shoppingListDataProvider.object(at: indexPath)
            shoppingListDataProvider.delete(shoppingList)
        }
    }
    
    // MARK: - ShoppingListDataProvider Delegate
    
    func shoppingListDataProviderDidInsert(at newIndexPath: IndexPath) {
        self.tableView.insertRows(at: [newIndexPath], with: .automatic)
    }
    
    func shoppingListDataProviderDidDelete(at indexPath: IndexPath) {
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    
}*/
