//
//  TableViewDataSource.swift
//  grocery_lean_controller
//
//  Created by Juan Francisco Dorado Torres on 7/31/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import CoreData

class TableViewDataSource<Cell: UITableViewCell, Model: NSManagedObject>: NSObject, UITableViewDataSource, FetchedResultsProviderDelegate where Model: ManagedObjectType {
    
    // MARK: - Properties
    
    var cellIdentifier: String
    var fetchedResultsProvider: FetchedResultsProvider<Model>
    var configureCell: (Cell, Model) -> ()
    var tableView: UITableView
    
    // MARK: - Initialization
    
    init(cellIdentifier: String, tableView: UITableView, fetchedResultsProvider: FetchedResultsProvider<Model>, configureCell: @escaping (Cell, Model) -> ()) {
        self.cellIdentifier = cellIdentifier
        self.tableView = tableView
        self.fetchedResultsProvider = fetchedResultsProvider
        self.configureCell = configureCell
        
        super.init()
        self.fetchedResultsProvider.delegate = self
    }
    
    // MARK: - UITableView DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsProvider.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchedResultsProvider.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? Cell else {
            return UITableViewCell()
        }
        
        let model = self.fetchedResultsProvider.object(at: indexPath)
        self.configureCell(cell, model)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let model = self.fetchedResultsProvider.object(at: indexPath)
            self.fetchedResultsProvider.delete(model)
        }
    }
    
    // MARK: - FetchedResultsProviderDelegate
    
    func fetchedResultsProviderDidInsert(at newIndexPath: IndexPath) {
        tableView.insertRows(at: [newIndexPath], with: .automatic)
    }
    
    func fetchedResultsProviderDidDelete(at indexPath: IndexPath) {
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
}
