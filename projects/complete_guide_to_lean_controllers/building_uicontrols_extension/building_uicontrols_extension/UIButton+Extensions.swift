//
//  UIButton+Extensions.swift
//  building_uicontrols_extension
//
//  Created by Juan Francisco Dorado Torres on 8/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

extension UIButton {
    
    static func buttonForAddingAnnotation() -> UIButton {
        let btn = UIButton(type: .system)
        btn.setTitle("Add Annotation", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
        
    }
}
