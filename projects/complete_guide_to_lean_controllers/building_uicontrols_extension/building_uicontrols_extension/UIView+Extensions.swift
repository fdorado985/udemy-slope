//
//  UIView+Extensions.swift
//  building_uicontrols_extension
//
//  Created by Juan Francisco Dorado Torres on 8/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

extension UIView {
    
    func addToParent(view: UIView) -> Self {
        view.addSubview(self)
        return self
    }
    
    func centerX(in view: UIView) -> Self {
        self.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        return self
    }
    
    func centerY(in view: UIView) -> Self {
        self.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        return self
    }
    
    func setWidth(width: CGFloat) -> Self {
        self.widthAnchor.constraint(equalToConstant: width)
        return self
    }
    
    func setHeight(height: CGFloat) -> Self {
        self.heightAnchor.constraint(equalToConstant: height)
        return self
    }
}
