//
//  ViewController.swift
//  building_uicontrols_extension
//
//  Created by Juan Francisco Dorado Torres on 8/1/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
    }
    
    // MARK: - Functions
    
    private func buildUI() {
        let _ = UIButton.buttonForAddingAnnotation()
            .addToParent(view: self.view)
            .centerX(in: self.view)
            .centerY(in: self.view)
            .setWidth(width: 100)
            .setHeight(height: 30)
        
        
//        self.view.addSubview(btn)
//        btn.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//        btn.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
//        btn.widthAnchor.constraint(equalToConstant: 100)
//        btn.heightAnchor.constraint(equalToConstant: 30)
    }


}

