//
//  Picture.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import RealmSwift
import Toucan

class Picture: Object {
    
    // MARK: Properties
    
    @objc dynamic var imageName = ""
    @objc dynamic var thumbName = ""
    @objc dynamic var entry: Entry?
    
    // MARK: Initialization
    
    convenience init(image: UIImage) {
        self.init()
        self.imageName = imageToURLString(image)
        if let smallImage = Toucan(image: image).resize(CGSize(width: 500, height: 500), fitMode: .crop).image {
            self.thumbName = imageToURLString(smallImage)
        }
    }
    
    // MARK: Functions
    
    func getImage() -> UIImage? {
        return image(with: imageName)
    }
    
    func getThumbnail() -> UIImage? {
        return image(with: thumbName)
    }
    
    private func imageToURLString(_ image: UIImage) -> String {
        if let imageData = UIImagePNGRepresentation(image) {
            let fileName = UUID().uuidString + ".png"
            
            if var path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                path.appendPathComponent(fileName)
                try? imageData.write(to: path)
            }
            return fileName
        }
        
        return ""
    }
    
    private func image(with fileName: String) -> UIImage? {
        guard var path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        path.appendPathComponent(fileName)
        guard let imageData = try? Data(contentsOf: path), let image = UIImage(data: imageData) else { return nil }
        return image
    }
}
