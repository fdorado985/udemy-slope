//
//  Entry.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import RealmSwift

class Entry: Object {
    
    // MARK: Properties
    
    @objc dynamic var text = ""
    @objc dynamic var date = Date()
    let pictures = List<Picture>()
    
    private var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        return formatter
    }()
    
    func getDate() -> String {
        return dateFormatter.string(from: self.date)
    }
    
    func getMonth() -> String {
        let formatter = self.dateFormatter
        formatter.dateFormat = "MMM"
        return formatter.string(from: date)
    }
    
    func getDay() -> String {
        let formatter = self.dateFormatter
        formatter.dateFormat = "d"
        return formatter.string(from: date)
    }
    
    func getYear() -> String {
        let formatter = self.dateFormatter
        formatter.dateFormat = "yyyy"
        return formatter.string(from: date)
    }
}
