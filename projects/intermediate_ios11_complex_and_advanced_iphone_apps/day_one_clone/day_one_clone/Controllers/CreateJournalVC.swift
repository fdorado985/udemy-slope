//
//  CreateJournalVC.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import RealmSwift
import Spring

class CreateJournalVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtJournal: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var stackBottom: UIStackView!
    @IBOutlet weak var stackDate: UIStackView!
    
    // MARK: Properties
    
    var startWithCamera = false
    private var imagePicker = UIImagePickerController()
    private var images = [UIImage]()
    private var entry = Entry()
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        imagePicker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateDate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if startWithCamera {
            startWithCamera = false
            takePhoto()
        }
    }
    
    // MARK: IBActions
    
    @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
        if let realm = try? Realm() {
            entry.text = txtJournal.text
            
            for image in images {
                let picture = Picture(image: image)
                entry.pictures.append(picture)
                picture.entry = entry
            }
            
            try? realm.write {
                realm.add(entry)
            }
            
            dismiss(animated: true)
        }
    }
    
    @IBAction func btnCancelTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction func btnSetDateTapped(_ sender: UIButton) {
        txtJournal.isHidden = false
        datePicker.isHidden = true
        btnDate.isHidden = true
        entry.date = datePicker.date
        updateDate()
    }
    
    @IBAction func btnCalendarTapped(_ sender: UIButton) {
        txtJournal.isHidden = true
        datePicker.isHidden = false
        btnDate.isHidden = false
        datePicker.date = entry.date
    }
    
    @IBAction func btnCameraTapped(_ sender: UIButton) {
        takePhoto()
    }
    
    // MARK: Functions
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        changeKeyboardHeight(notification)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        changeKeyboardHeight(notification)
    }
    
    private func changeKeyboardHeight(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
                return
        }
        
        let keyboardHeight = keyboardFrame.cgRectValue.height
        bottomConstraint.constant = keyboardHeight + 10
    }
    
    private func updateDate() {
        title = entry.getDate()
    }
    
    private func takePhoto() {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true)
    }
}

// MARK: - UIImagePickerController && UINavigationController Delegates

extension CreateJournalVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            imagePicker.dismiss(animated: true)
            return
        }
        images.append(chosenImage)
        
        let imageView = SpringImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: stackBottom.frame.size.height).isActive = true
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1 / 1).isActive = true
        imageView.image = chosenImage
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        stackBottom.addArrangedSubview(imageView)
        imagePicker.dismiss(animated: true) {
            // Animation
            imageView.animation = "pop"
            imageView.duration = 2.0
            imageView.animate()
        }
    }
}
