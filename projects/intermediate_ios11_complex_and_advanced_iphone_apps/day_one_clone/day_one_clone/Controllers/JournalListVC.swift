//
//  JournalListVC.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import RealmSwift

class JournalListVC: UITableViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    
    // MARK: Properties
    
    private var entries: Results<Entry>?
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCamera.imageView?.contentMode = .scaleAspectFit
        btnAdd.imageView?.contentMode = .scaleAspectFit
        
        //self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow") // Can use it in storyboard instead
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadEntries()
    }
    
    // MARK: IBActions
    
    @IBAction func btnCameraTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "CreateJournalVC", sender: "camera")
    }
    
    @IBAction func btnAddTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "CreateJournalVC", sender: nil)
    }
    
    // MARK: Functions
    
    private func loadEntries() {
        if let realm = try? Realm() {
            self.entries = realm.objects(Entry.self).sorted(byKeyPath: "date", ascending: false)
            tableView.reloadData()
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateJournalVC",
            let navigation = segue.destination as? UINavigationController,
            let vc = navigation.viewControllers.first as? CreateJournalVC {
            if let source = sender as? String, source == "camera" {
                vc.startWithCamera = true
            }
        } else if segue.identifier == "JournalDetailVC",
            let navigation = segue.destination as? UINavigationController,
            let vc = navigation.viewControllers.first as? JournalDetailVC {
            if let entry = sender as? Entry {
                vc.entry = entry
            }
        }
    }
}

// MARK: - TableView Delegate|Datasource

extension JournalListVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let entries = self.entries else { return 0 }
        return entries.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JournalCell", for: indexPath) as? JournalCell else { return UITableViewCell() }
        
        if let entries = self.entries {
            let entry = entries[indexPath.row]
            cell.txtJournal.text = entry.text
            cell.lblMonth.text = entry.getMonth()
            cell.lblDay.text = entry.getDay()
            cell.lblYear.text = entry.getYear()
            
            if let image = entry.pictures.first?.getThumbnail() {
                cell.imgJournal.image = image
            } else {
                cell.imgJournal.isHidden = true
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let entries = self.entries {
            let entry = entries[indexPath.row]
            performSegue(withIdentifier: "JournalDetailVC", sender: entry)
        }
    }
}
