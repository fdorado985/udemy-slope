//
//  PhotoListVC.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import RealmSwift

private let reuseIdentifier = "PhotoCell"

class PhotoListVC: UICollectionViewController {
    
    // MARK: Properties
    
    private var pictures: Results<Picture>?
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadPictures()
    }
    
    // MARK: Functions
    
    private func loadPictures() {
        if let realm = try? Realm() {
            pictures = realm.objects(Picture.self)
            collectionView?.reloadData()
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "JournalDetailVC",
            let navigation = segue.destination as? UINavigationController,
            let vc = navigation.viewControllers.first as? JournalDetailVC {
            if let entry = sender as? Entry {
                vc.entry = entry
            }
        }
    }
}

// MARK: - UICollectionView DataSource

extension PhotoListVC {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let pictures = self.pictures else { return 0 }
        return pictures.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? PhotoCell else { fatalError("PhotoCell couldn't be created") }
        
        // Configure the cell
        if let pictures = self.pictures {
            let picture = pictures[indexPath.row]
            cell.imgPreview.image = picture.getThumbnail()
            
            if let entry = picture.entry {
                cell.dateDay.text = entry.getDay()
                cell.dateMonth.text = "\(entry.getMonth()) \(entry.getYear())"
            }
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let pictures = self.pictures {
            let picture = pictures[indexPath.row]
            if let entry = picture.entry {
                performSegue(withIdentifier: "JournalDetailVC", sender: entry)
            }
        }
    }
}

extension PhotoListVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2, height: collectionView.frame.size.width / 2)
    }
}
