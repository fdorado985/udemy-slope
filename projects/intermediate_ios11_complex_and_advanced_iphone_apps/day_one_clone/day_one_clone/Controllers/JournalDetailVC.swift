//
//  JournalDetailVC.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/13/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class JournalDetailVC: UIViewController {
    
    // MARK: IBOutlets

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var txtJournal: UILabel!
    
    // MARK: Properties
    
    var entry: Entry?
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let entry = self.entry {
            title = entry.getDate()
            txtJournal.text = entry.text
            
            for picture in entry.pictures {
                let imageView = UIImageView()
                imageView.contentMode = .scaleAspectFit
                if let fullImage = picture.getImage() {
                    let ratio = fullImage.size.height / fullImage.size.width
                    imageView.widthAnchor.constraint(equalTo: stackView.widthAnchor, multiplier: 1.0)
                    imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: ratio).isActive = true
                    imageView.image = fullImage
                    stackView.addArrangedSubview(imageView)
                }
            }
        }
    }
}
