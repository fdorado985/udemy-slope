//
//  JournalCell.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class JournalCell: UITableViewCell {

    // MARK: IBOutlets
    
    @IBOutlet weak var txtJournal: UILabel!
    @IBOutlet weak var imgJournal: UIImageView!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblYear: UILabel!

}
