//
//  PhotoCell.swift
//  day_one_clone
//
//  Created by Juan Francisco Dorado Torres on 8/14/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgPreview: UIImageView!
    @IBOutlet weak var dateDay: UILabel!
    @IBOutlet weak var dateMonth: UILabel!
}
