#  Journal Day [Day One Clone]
## Description
This is a simple app that works as a basic clone of the popular `Day One` app... you can save entries with a description and the photos you want in that entry... also change the date...

## Wrap Up
Here you will see an app that works with....
* Animations using Spring
* Database using Realm
* UISplitViewController

## Demo

![day_one_clone_demo](screenshots/day_one_clone_demo.gif)
