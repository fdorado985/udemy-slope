//
//  PicturesListVC.swift
//  sketchapp
//
//  Created by Juan Francisco Dorado Torres on 8/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

private let reuseIdentifier = "PictureCell"

class PicturesListVC: UICollectionViewController {
    
    // MARK: Properties
    
    private var pictures = [Picture]()
    
    // MARK: View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadPictures()
    }
    
    // MARK: Functions
    
    private func loadPictures() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { fatalError("AppDelegate could not be reached") }
        let context = appDelegate.persistentContainer.viewContext
        if let fetchedPictures = try? context.fetch(Picture.fetchRequest()) as? [Picture] {
            self.pictures = fetchedPictures ?? []
            self.collectionView?.reloadData()
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PictureDetailVC",
            let vc = segue.destination as? PictureDetailVC,
            let picture = sender as? Picture {
            vc.picture = picture
        }
    }
}

// MARK: - UICollectionView DataSource

extension PicturesListVC {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? PictureCell else { fatalError("PictureCell couldn't be reached") }
        
        // Configure the cell
        let picture = pictures[indexPath.row]
        cell.configureCell(picture)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let picture = pictures[indexPath.row]
        self.performSegue(withIdentifier: "PictureDetailVC", sender: picture)
    }
}
