//
//  PictureDetailVC.swift
//  sketchapp
//
//  Created by Juan Francisco Dorado Torres on 8/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PictureDetailVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak private var imgPicture: UIImageView!
    
    // MARK: Properties
    
    var picture: Picture?

    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPicture()
    }
    
    // MARK: IBActions
    
    @IBAction func btnShareTapped(_ sender: UIBarButtonItem) {
        guard let picture = imgPicture.image else { return }
        let shareVC = UIActivityViewController(activityItems: [picture], applicationActivities: nil)
        self.present(shareVC, animated: true)
    }
    
    @IBAction func btnDeleteTapped(_ sender: UIBarButtonItem) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { fatalError("App Delegate couldn't be reached") }
        guard let picture = self.picture else { return }
        let context = appDelegate.persistentContainer.viewContext
        context.delete(picture)
        appDelegate.saveContext()
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Functions
    
    private func loadPicture() {
        guard let picture = self.picture,
            let imgData = picture.image else { return }
        
        imgPicture.image = UIImage(data: imgData, scale: 1)
        self.title = picture.name
    }

}
