//
//  PictureCell.swift
//  sketchapp
//
//  Created by Juan Francisco Dorado Torres on 8/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class PictureCell: UICollectionViewCell {
    
    // MARK: IBOutlets
    
    @IBOutlet weak private var imgPicture: UIImageView!
    @IBOutlet weak private var lblName: UILabel!
    
    // MARK: Functions
    
    func configureCell(_ picture: Picture) {
        self.lblName.text = picture.name
        
        guard let imgData = picture.image else { return }
        self.imgPicture.image = UIImage(data: imgData)
    }
}
