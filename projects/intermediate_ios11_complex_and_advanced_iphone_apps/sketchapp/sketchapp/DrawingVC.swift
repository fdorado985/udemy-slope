//
//  DrawingVC.swift
//  sketchapp
//
//  Created by Juan Francisco Dorado Torres on 8/15/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import ChromaColorPicker

class DrawingVC: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var imgDraw: UIImageView!
    @IBOutlet weak var stackTools: UIStackView!
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var btnSize: UIButton!
    @IBOutlet weak var btnEraser: UIButton!
    
    // MARK: Properties
    
    private var lastPoint = CGPoint(x: 0, y: 0)
    private var brushColor = UIColor.blue.cgColor
    private var brushSize: Float = 30.0
    private var colorPicker: ChromaColorPicker?
    private var greyedOut = UIView()
    private var isErasing = false
    
    // MARK: View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Drawing..."
        
        self.btnColor.imageView?.contentMode = .scaleAspectFit
        self.btnSize.imageView?.contentMode = .scaleAspectFit
        self.btnEraser.imageView?.contentMode = .scaleAspectFit
        
        self.greyedOut = UIView(frame: view.frame)
        self.greyedOut.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.view.addSubview(self.greyedOut)
        
        self.colorPicker = ChromaColorPicker(frame: CGRect(x: view.frame.size.width / 2 - 100, y: view.frame.size.height / 2 - 100, width: 200, height: 200))
        if let picker = colorPicker {
            picker.delegate = self
            picker.padding = 5
            picker.stroke = 3
            picker.hexLabel.isHidden = true
            view.addSubview(picker)
        }
        self.colorPicker?.isHidden = true
        self.greyedOut.isHidden = true
    }
    
    // MARK: IBActions
    
    @IBAction func btnColorTapped(_ sender: UIButton) {
        self.colorPicker?.adjustToColor(UIColor(cgColor: self.brushColor))
        self.colorPicker?.isHidden = false
        self.greyedOut.isHidden = false
    }
    
    @IBAction func btnSizeTapped(_ sender: UIButton) {
        let ac = UIAlertController(title: "Brush Size", message: "\n\n\n\n\n\n\n", preferredStyle: .alert)
        let slider = UISlider(frame: CGRect(x: 10, y: 50, width: 250, height: 80))
        slider.minimumValue = 1
        slider.maximumValue = 100
        slider.value = brushSize
        ac.view.addSubview(slider)
        let btnAccept = UIAlertAction(title: "Accept", style: .default) { [weak self] (alert) in
            if let strongSelf = self {
                strongSelf.brushSize = slider.value
            }
        }
        ac.addAction(btnAccept)
        self.present(ac, animated: true)
    }
    
    @IBAction func btnEraserTapped(_ sender: UIButton) {
        self.isErasing = !self.isErasing
        self.title = self.isErasing ? "Erasing..." : "Drawing..."
    }
    
    @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
        let ac = UIAlertController(title: "Save Image", message: "Name your picture", preferredStyle: .alert)
        ac.addTextField { (textField) in
            textField.placeholder = "My Masterpiece"
        }
        let btnSave = UIAlertAction(title: "Save", style: .default) { [weak self] (action) in
            if let strongSelf = self {
                if let name = ac.textFields?.first?.text, !name.isEmpty {
                    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                        let context = appDelegate.persistentContainer.viewContext
                        let picture = Picture(context: context)
                        picture.name = name
                        if let image = strongSelf.imgDraw.image {
                            picture.image = UIImageJPEGRepresentation(image, 1)
                            appDelegate.saveContext()
                        }
                    }
                    strongSelf.navigationController?.popViewController(animated: true)
                }
            }
            
        }
        let btnCancel = UIAlertAction(title: "Cancel", style: .destructive)
        ac.addAction(btnSave)
        ac.addAction(btnCancel)
        self.present(ac, animated: true)
    }
    
    // MARK: Functions
    
    private func drawBetween(start: CGPoint, end: CGPoint) {
        UIGraphicsBeginImageContext(self.imgDraw.frame.size)
        self.imgDraw.image?.draw(in: CGRect(x: 0, y: 0, width: self.imgDraw.frame.size.width, height: self.imgDraw.frame.size.height))
        
        if let context = UIGraphicsGetCurrentContext() {
            context.move(to: start)
            context.addLine(to: end)
            context.setLineWidth(CGFloat(self.brushSize) / 3.0)
            context.setLineCap(.round)
            context.setStrokeColor(self.isErasing ? UIColor.white.cgColor : self.brushColor)
            context.strokePath()
            
            self.imgDraw.image = UIGraphicsGetImageFromCurrentImageContext()
        }
        
        UIGraphicsEndImageContext()
    }
    
    // MARK: Touches
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        stackTools.isHidden = true
        if let startPoint = touches.first?.location(in: self.imgDraw) {
            self.lastPoint = startPoint
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let movedPoint = touches.first?.location(in: self.imgDraw) {
            self.drawBetween(start: self.lastPoint, end: movedPoint)
            self.lastPoint = movedPoint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        stackTools.isHidden = false
        if let endPoint = touches.first?.location(in: imgDraw) {
            self.drawBetween(start: self.lastPoint, end: endPoint)
        }
    }

}

extension DrawingVC: ChromaColorPickerDelegate {
    
    func colorPickerDidChooseColor(_ colorPicker: ChromaColorPicker, color: UIColor) {
        self.brushColor = color.cgColor
        colorPicker.isHidden = true
        self.greyedOut.isHidden = true
    }
}
