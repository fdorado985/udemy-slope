#  Sketchpad

## Description
This is a drawing app... this will work with touches handler, you will see how to draw, use collectionViews, UIImageContext, add a colorPicker, UIActivityViewController, and simple and basic use of CoreData.

## Demo
![sketchpad_demo](screenshots/sketchpad_demo.gif)
