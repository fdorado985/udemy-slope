#  NEWS FUN

On this app we use CoreML through third parties, with CocoaPods, let's get some news, from an API and use CoreML to catch the category through the description on the news.

## Demo

![demo_news_fun](screenshots/demo_news_fun.gif)
