//
//  NewsHelper.swift
//  news-fun
//
//  Created by Juan Francisco Dorado Torres on 5/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire
import DocumentClassifier

class NewsService {

  // MARK: - Singleton

  static let shared = NewsService()

  // MARK: - Constants

  private let NEWS_API_KEY = "12e8374382914d9290af7da6c3046979"
  private let NEWS_URL = "https://newsapi.org/v2/"

  // MARK: - Enums

  private enum NEWS_FUNCTIONS: String {
    case topHeadlines = "top-headlines"
  }

  private enum NEWS_PARAMETERS: String {
    case country = "country=%@"
    case apiKey = "apiKey=%@"
  }

  // MARK: - Private Functions

  private func createURL(url: String, to function: NEWS_FUNCTIONS, with parameters: [String], and arguments: [String]) -> String {
    let joinedParameters = parameters.joined(separator: "&")
    let url = "\(url)\(function.rawValue)?\(joinedParameters)"
    let fullURL = String(format: url, arguments: arguments)
    return fullURL
  }

  // MARK: - Private Functions

  private func getCategory(for classification: Classification.Category) -> (name: NEWS_CATEGORY, color: UIColor) {
    switch classification {
    case .business:
      let color = UIColor(red: 0.298, green: 0.882, blue: 0.949, alpha: 1.00)
      return (.business, color)
    case .entertainment:
      let color = UIColor(red: 0.129, green: 0.788, blue: 0.588, alpha: 1.00)
      return (.entertainment, color)
    case .politics:
      let color = UIColor(red: 0.929, green: 0.667, blue: 0.169, alpha: 1.00)
      return (.politics, color)
    case .sports:
      let color = UIColor(red: 0.996, green: 0.847, blue: 0.325, alpha: 1.00)
      return (.sports, color)
    case .technology:
      let color = UIColor(red: 0.949, green: 0.396, blue: 0.220, alpha: 1.00)
      return (.technology, color)
    }
  }

  // MARK: - Functions

  func getArticles(_ handler: @escaping ([Article]) -> ()) {
    let parameters = [NEWS_PARAMETERS.country.rawValue, NEWS_PARAMETERS.apiKey.rawValue]
    let newsURL = createURL(url: NEWS_URL, to: .topHeadlines, with: parameters, and: ["us", NEWS_API_KEY])
    Alamofire.request(newsURL).responseJSON { (response) in
      var articles = [Article]()
      if let json = response.result.value as? [String : Any], let jsonArticles = json["articles"] as? [[String : Any]] {
        jsonArticles.forEach({
          guard let title = $0["title"] as? String,
            let urlToImage = $0["urlToImage"] as? String,
            let url = $0["url"] as? String,
            let description = $0["description"] as? String else { return }

          guard let classification = DocumentClassifier().classify(description) else { return }
          let category = self.getCategory(for: classification.prediction.category)

          let article = Article(
            title: title,
            urlToImage: urlToImage,
            url: url,
            description: description,
            category: category
          )
          articles.append(article)
        })
      }
      handler(articles)
    }
  }
}
