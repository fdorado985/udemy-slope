//
//  Article.swift
//  news-fun
//
//  Created by Juan Francisco Dorado Torres on 5/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

enum NEWS_CATEGORY: String {
  case business = "🗄 Business"
  case entertainment = "🎭 Entertainment"
  case politics = "🇲🇽 Politics"
  case sports = "⚽️ Sports"
  case technology = "🖥 Technology"
}

class Article {

  let title: String
  let urlToImage: String
  let url: String
  let description: String
  let category: (name: NEWS_CATEGORY, color: UIColor)

  init(title: String, urlToImage: String, url: String, description: String, category: (NEWS_CATEGORY, UIColor)) {
    self.title = title
    self.urlToImage = urlToImage
    self.url = url
    self.description = description
    self.category = category
  }
}
