//
//  ArticleVC.swift
//  news-fun
//
//  Created by Juan Francisco Dorado Torres on 5/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import WebKit

class ArticleVC: UIViewController {

  // MARK: - IBOutlet

  @IBOutlet weak var webView: WKWebView!

  // MARK - Properties

  var article: Article?

  // MARK: - View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    loadArticle()
  }

  // MARK: - Functions

  private func loadArticle() {
    guard let article = article, let url = URL(string: article.url) else { return }
    webView.load(URLRequest(url: url))
  }

}
