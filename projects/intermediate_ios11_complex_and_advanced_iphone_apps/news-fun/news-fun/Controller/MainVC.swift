//
//  MainVC.swift
//  news-fun
//
//  Created by Juan Francisco Dorado Torres on 5/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class MainVC: UITableViewController {

  // MARK: - Constants

  private let newsService = NewsService.shared

  // MARK: - Properties

  private var articles = [Article]()

  // MARK: - View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    getArticles()
  }

  // MARK: - IBOutlets

  @IBAction func btnReloadTapped(_ sender: UIBarButtonItem) {
    getArticles()
  }

  // MARK: - Functions

  private func getArticles() {
    newsService.getArticles {
      self.articles = $0
      self.tableView.reloadData()
    }
  }

  // MARK: - Perform Segue

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "article_vc",
      let article = sender as? Article,
      let articleVC = segue.destination as? ArticleVC {
      articleVC.article = article
    }
  }
}

// MARK: - UITableView Delegate|DataSource

extension MainVC {

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return articles.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "article_cell", for: indexPath) as? ArticleCell else { return UITableViewCell() }
    let article = articles[indexPath.row]
    cell.configureCell(with: article)
    return cell
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let article = articles[indexPath.row]
    performSegue(withIdentifier: "article_vc", sender: article)
  }
}
