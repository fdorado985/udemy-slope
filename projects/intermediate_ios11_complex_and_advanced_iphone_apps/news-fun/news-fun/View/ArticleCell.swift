//
//  ArticleCell.swift
//  news-fun
//
//  Created by Juan Francisco Dorado Torres on 5/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleCell: UITableViewCell {

  @IBOutlet weak var title: UILabel!
  @IBOutlet weak var imgArticle: UIImageView!
  @IBOutlet weak var category: UILabel!

  func configureCell(with article: Article) {
    self.title.text = article.title
    self.category.text = article.category.name.rawValue
    self.category.backgroundColor = article.category.color
    let url = URL(string: article.urlToImage)
    imgArticle.kf.setImage(with: url, placeholder: UIImage(named: "Filler"), options: nil, progressBlock: nil, completionHandler: nil)
  }
}
