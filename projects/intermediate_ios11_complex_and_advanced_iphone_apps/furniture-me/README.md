#  Furniture Me

On this app you will be able to see, how using ARKit, we can add some models as Augmented Reality, on this case, a chair, you are gonna be able to rotate it, make it bigger, smaller, and move it through your scene.

## Demo
![demo_furniture_me](screenshots/demo_furniture_me.gif)
