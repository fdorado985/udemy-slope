//
//  MainVC.swift
//  furniture-me
//
//  Created by Juan Francisco Dorado Torres on 5/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import MBProgressHUD

class MainVC: UIViewController, ARSCNViewDelegate {

  // MARK: - IBOutlets

  @IBOutlet var sceneView: ARSCNView!

  // MARK: - Properties

  private var hud: MBProgressHUD!
  private var currentAngleY: Float = 0.0
  private var newAngleY: Float = 0.0
  private var localTranslatePosition: CGPoint!

  // MARK: - View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()

    sceneView.autoenablesDefaultLighting = true

    hud = MBProgressHUD.showAdded(to: self.sceneView, animated: true)
    hud.label.text = "Detecting plane..."

    // Set the view's delegate
    sceneView.delegate = self

    // Show statistics such as fps and timing information
    sceneView.showsStatistics = true

    // Create a new scene
    let scene = SCNScene()

    // Set the scene to the view
    sceneView.scene = scene

    // Register Gestures
    registerGestureRecognizers()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    // Create a session configuration
    let configuration = ARWorldTrackingConfiguration()
    configuration.planeDetection = .horizontal

    // Run the view's session
    sceneView.session.run(configuration)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    // Pause the view's session
    sceneView.session.pause()
  }
}

// MARK: - ARKit

extension MainVC {

  func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    if anchor is ARPlaneAnchor {
      DispatchQueue.main.async {
        self.hud.label.text = "Plane detected!"
        self.hud.hide(animated: true, afterDelay: 1.0)
      }
    }
  }
}

// MARK: - Functions

extension MainVC {

  private func registerGestureRecognizers() {
    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(userTapped(_:)))
    sceneView.addGestureRecognizer(tapGestureRecognizer)

    let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(userPinched(_:)))
    sceneView.addGestureRecognizer(pinchGestureRecognizer)

    let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(userPanned(_:)))
    sceneView.addGestureRecognizer(panGestureRecognizer)

    let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(userLongPressed(_:)))
    sceneView.addGestureRecognizer(longPressGestureRecognizer)
  }

  @objc private func userTapped(_ sender: UITapGestureRecognizer) {
    guard let sceneView = sender.view as? ARSCNView else { return }

    let touchLocation = sender.location(in: sceneView)
    let hitTestResults = sceneView.hitTest(touchLocation, types: .existingPlane)

    guard let hitTest = hitTestResults.first,
      let chairScene = SCNScene(named: "art.scnassets/chair.dae"),
      let chairNode = chairScene.rootNode.childNode(withName: "parentNode", recursively: true) else {
        return
    }

    chairNode.position = SCNVector3(
      hitTest.worldTransform.columns.3.x,
      hitTest.worldTransform.columns.3.y,
      hitTest.worldTransform.columns.3.z
    )

    sceneView.scene.rootNode.addChildNode(chairNode)
  }

  @objc private func userPinched(_ sender: UIPinchGestureRecognizer) {
    switch sender.state {
    case .changed:
      guard let sceneView = sender.view as? ARSCNView else { return }
      let touchLocation = sender.location(in: sceneView)
      let hitTestResults = sceneView.hitTest(touchLocation, options: nil)

      guard let hitTest = hitTestResults.first else { return }
      let chairNode = hitTest.node

      let pinchScaleX = Float(sender.scale) * chairNode.scale.x
      let pinchScaleY = Float(sender.scale) * chairNode.scale.y
      let pinchScaleZ = Float(sender.scale) * chairNode.scale.z

      chairNode.scale = SCNVector3(pinchScaleX, pinchScaleY, pinchScaleZ)
      sender.scale = 1
    default:
      break
    }

  }

  @objc private func userPanned(_ sender: UIPanGestureRecognizer) {
    switch sender.state {
    case .changed:
      guard let sceneView = sender.view as? ARSCNView else { return }
      let touchLocation = sender.location(in: sceneView)
      let translation = sender.translation(in: sceneView)
      let hitTestResults = sceneView.hitTest(touchLocation, options: nil)

      guard let hitTest = hitTestResults.first,
        let parentNode = hitTest.node.parent else { return }

      newAngleY = Float(translation.x) * Float(Double.pi / 180)
      newAngleY += currentAngleY
      parentNode.eulerAngles.y = newAngleY
    case .ended:
      currentAngleY = newAngleY
    default:
      break
    }
  }

  @objc private func userLongPressed(_ sender: UILongPressGestureRecognizer) {
    guard let sceneView = sender.view as? ARSCNView else { return }
    let touchLocation = sender.location(in: sceneView)
    let hitTestResults = sceneView.hitTest(touchLocation, options: nil)

    guard let hitTest = hitTestResults.first,
      let parentNode = hitTest.node.parent else { return }

    switch sender.state {
    case .began:
      localTranslatePosition = touchLocation
    case .changed:
      let deltaX = Float(touchLocation.x - localTranslatePosition.x) / 700
      let deltaY = Float(touchLocation.y - localTranslatePosition.y) / 700

      parentNode.localTranslate(by: SCNVector3(deltaX, 0.0, deltaY))
      localTranslatePosition = touchLocation
    default:
      break
    }

  }
}
