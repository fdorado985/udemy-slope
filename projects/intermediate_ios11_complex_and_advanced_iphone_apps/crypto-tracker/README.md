#  Crypto Tracker

This app will track the value of differents kind of Cryptocurrencies, also you can take any of them and add an amount of your own in case you have it, to calculate how much value does it has, you can verify on three different currencies... USD (Dollar), EUR (Euro), MXN (Mexican Peso).

This app contains security to let open the app using biometrics (Face|Touch ID)

And to finish... you can take a report of your Net Worth to get a PDF or even share it by any social media.

## Demo

![demo_crypto_tracker](screenshots/demo_crypto_tracker.gif)
![demo_biometric_crypto_tracker](screenshots/demo_biometric_crypto_tracker.gif)
