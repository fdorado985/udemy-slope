//
//  Double+Ext.swift
//  crypto-tracker
//
//  Created by Juan Francisco Dorado Torres on 5/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

extension Double {

  var eurCurrency: String {
    return CurrencyFormatter(type: .EUR)
  }

  var mxnCurrency: String {
    return CurrencyFormatter(type: .MXN)
  }

  var usdCurrency: String {
    return CurrencyFormatter(type: .USD)
  }


  private enum CurrencyType: String {
    case USD = "en_US_POSIX"
    case EUR = "es_ES"
    case MXN = "es_MX"
  }

  private func CurrencyFormatter(type: CurrencyType) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    formatter.minimumFractionDigits = 2
    formatter.maximumFractionDigits = 2
    formatter.locale = Locale(identifier: type.rawValue)
    return formatter.string(from: NSNumber(value: self)) ?? "0.00"
  }
}
