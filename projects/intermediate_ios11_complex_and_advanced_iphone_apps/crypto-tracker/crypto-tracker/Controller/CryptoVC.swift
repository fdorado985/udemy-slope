//
//  CryptoVC.swift
//  crypto-tracker
//
//  Created by Juan Francisco Dorado Torres on 5/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import LocalAuthentication

class CryptoVC: UITableViewController {


  // MARK: - Properties

  private var lblAmount = UILabel()
  private var btnCurrency = UIBarButtonItem()
  private var currencyTypeSelected = CurrencyType.USD

  // MARK: - Constants

  private let cryptoData = CryptoData.shared
  private let headerHeight: CGFloat = 100.0
  private let networthHeight: CGFloat = 45.0
  private let amountHeight: CGFloat = 55.0

  // MARK: - View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = "Cryto-Tracker"
    tableView.register(UINib(nibName: "CryptocurrencyCell", bundle: nil), forCellReuseIdentifier: "cryptocurrencycell")
    addBarButtonItem()
    cryptoData.getPrices()
    cryptoData.delegate = self
    verifyBiometric()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    cryptoData.delegate = self
    displayNetWorth()
    tableView.reloadData()
  }

  // MARK: - Functions

  private func verifyBiometric() {
    if LAContext().canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
      addAndUpdateSecureButton()
    }
  }

  private func addBarButtonItem() {
    btnCurrency = UIBarButtonItem(title: "USD", style: .plain, target: self, action: #selector(btnCurrencyTapped(_:)))
    let btnReport = UIBarButtonItem(title: "Report", style: .plain, target: self, action: #selector(btnReportTapped(_:)))
    navigationItem.rightBarButtonItems = [btnCurrency, btnReport]
  }

  private func addAndUpdateSecureButton() {
    let titleForButton = userDefaults.bool(forKey: "secure_key") ? "Unsecure App" : "Secure App"
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: titleForButton, style: .plain, target: self, action: #selector(btnSecureTapped(_:)))
  }

  private func createHeaderView() -> UIView {
    let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: headerHeight))
    headerView.backgroundColor = UIColor.white

    let networthLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: networthHeight))
    networthLabel.text = "My Crypto Net Worth:"
    networthLabel.textAlignment = .center

    lblAmount.frame = CGRect(x: 0, y: networthHeight, width: view.frame.size.width, height: amountHeight)
    lblAmount.textAlignment = .center
    lblAmount.adjustsFontSizeToFitWidth = true
    lblAmount.font = UIFont.boldSystemFont(ofSize: 60)

    headerView.addSubview(networthLabel)
    headerView.addSubview(lblAmount)
    return headerView
  }

  private func displayNetWorth() {
    lblAmount.text = cryptoData.netWorthAsString(for: currencyTypeSelected)
  }

  @objc private func btnSecureTapped(_ sender: UIBarButtonItem) {
    let currentSecureStatus = userDefaults.bool(forKey: "secure_key")
    userDefaults.set(!currentSecureStatus, forKey: "secure_key")
    addAndUpdateSecureButton()
  }

  @objc private func btnCurrencyTapped(_ sender: UIBarButtonItem) {
    switch currencyTypeSelected {
    case .USD:
      currencyTypeSelected = .EUR
      sender.title = "EUR"
    case .EUR:
      currencyTypeSelected = .MXN
      sender.title = "MXN"
    case .MXN:
      currencyTypeSelected = .USD
      sender.title = "USD"
    }

    displayNetWorth()
  }

  @objc func btnReportTapped(_ sender: UIBarButtonItem) {
    let formatter = UIMarkupTextPrintFormatter(markupText: cryptoData.html(for: currencyTypeSelected))
    
    let render = UIPrintPageRenderer()
    render.addPrintFormatter(formatter, startingAtPageAt: 0)

    let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8)
    render.setValue(page, forKey: "paperRect")
    render.setValue(page, forKey: "printableRect")

    let pdfData = NSMutableData()
    UIGraphicsBeginPDFContextToData(pdfData, .zero, nil)
    for i in 0..<render.numberOfPages {
      UIGraphicsBeginPDFPage()
      render.drawPage(at: i, in: UIGraphicsGetPDFContextBounds())
    }
    UIGraphicsEndPDFContext()

    let shareVC = UIActivityViewController(activityItems: [pdfData], applicationActivities: nil)
    present(shareVC, animated: true, completion: nil)
  }
}

// MARK: - UITableViewControllerDelegate|UITableViewDataSource

extension CryptoVC {

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cryptoData.currencies.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "cryptocurrencycell", for: indexPath) as? CryptocurrencyCell else { return UITableViewCell() }

    let currency = cryptoData.currencies[indexPath.row]
    cell.configureCell(currency: currency)

    return cell
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let currencyVC = CurrencyVC()
    currencyVC.currency = cryptoData.currencies[indexPath.row]
    navigationController?.pushViewController(currencyVC, animated: true)
  }

  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 123
  }

  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return headerHeight
  }

  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    return createHeaderView()
  }
}

// MARK: - CryptoDataDelegate

extension CryptoVC : CryptoDataDelegate {

  func newPrices() {
    displayNetWorth()
    tableView.reloadData()
  }
}
