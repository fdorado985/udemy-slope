//
//  CurrencyVC.swift
//  crypto-tracker
//
//  Created by Juan Francisco Dorado Torres on 5/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import SwiftChart

class CurrencyVC: UIViewController {

  // MARK: - Properties

  var currency: Currency?
  private var chart = Chart()
  private var stepper = UISegmentedControl()
  private var cryptoLogo = UIImageView()
  private var lblCryptoPrice = UILabel()
  private var lblCryptoAccount = UILabel()
  private var lblCryptoAccountConverted = UILabel()
  private var stackCryptoData = UIStackView()
  private var stackCryptoAccount = UIStackView()
  private var navBarHeight: CGFloat = 0.0
  private var currencyType = CurrencyType.USD

  // MARK: - Constants

  private let cryptoData = CryptoData.shared
  private let chartHeight: CGFloat = 300.0
  private let segmentHeight: CGFloat = 30.0

  // MARK: - View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = currency?.symbol
    view.backgroundColor = UIColor.white
    cryptoData.delegate = self
    setupView()
  }

  // MARK: - Functions

  private func setupView() {
    navBarHeight = navigationController?.navigationBar.frame.height ?? 0.0
    addBarButtonItems()
    addStepper()
    addChart()
    addCryptoData()

    loadData()
  }

  private func addBarButtonItems() {
    navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editTapped(_:)))
  }

  private func addStepper() {
    stepper = UISegmentedControl(items: ["USD", "EUR", "MXN"])
    stepper.selectedSegmentIndex = 0
    stepper.addTarget(self, action: #selector(segmentDidChangeValue(_:)), for: .valueChanged)
    view.addSubview(stepper)

    // Add Constraints
    stepper.translatesAutoresizingMaskIntoConstraints = false
    let leadingSpace = NSLayoutConstraint(item: stepper, attribute: .leadingMargin, relatedBy: .equal, toItem: view, attribute: .leadingMargin, multiplier: 1.0, constant: 20)
    let trailingSpace = NSLayoutConstraint(item: stepper, attribute: .trailingMargin, relatedBy: .equal, toItem: view, attribute: .trailingMargin, multiplier: 1.0, constant: -20)
    let topSpace = NSLayoutConstraint(item: stepper, attribute: .topMargin, relatedBy: .equal, toItem: view, attribute: .topMargin, multiplier: 1.0, constant: 20)
    let heightConstraint = NSLayoutConstraint(item: stepper, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: segmentHeight)
    NSLayoutConstraint.activate([leadingSpace, trailingSpace, topSpace, heightConstraint])
  }

  private func addChart() {
    chart.yLabelsFormatter = { (intValue, doubleValue) in
      switch self.currencyType {
      case .USD:
        return doubleValue.usdCurrency
      case .EUR:
        return doubleValue.eurCurrency
      case .MXN:
        return doubleValue.mxnCurrency
      }
    }
    chart.xLabels = [0, 5, 10, 15, 20, 25, 30]
    chart.xLabelsFormatter = { String(Int(round(30 - $1))) + "d" }
    view.addSubview(chart)

    // Add Constraints
    chart.translatesAutoresizingMaskIntoConstraints = false
    let leadingSpace = NSLayoutConstraint(item: chart, attribute: .leadingMargin, relatedBy: .equal, toItem: view, attribute: .leadingMargin, multiplier: 1.0, constant: 20)
    let trailingSpace = NSLayoutConstraint(item: chart, attribute: .trailingMargin, relatedBy: .equal, toItem: view, attribute: .trailingMargin, multiplier: 1.0, constant: -20)
    let topSpace = NSLayoutConstraint(item: chart, attribute: .topMargin, relatedBy: .equal, toItem: stepper, attribute: .bottom, multiplier: 1.0, constant: 20)
    let heightConstraint = NSLayoutConstraint(item: chart, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: chartHeight)
    NSLayoutConstraint.activate([leadingSpace, trailingSpace, topSpace, heightConstraint])
  }

  private func addCryptoData() {
    lblCryptoAccount.textAlignment = .center
    lblCryptoAccount.font = UIFont.boldSystemFont(ofSize: 20.0)
    lblCryptoAccountConverted.textAlignment = .center
    lblCryptoAccountConverted.font = UIFont.boldSystemFont(ofSize: 20.0)

    cryptoLogo.contentMode = .scaleAspectFit
    cryptoLogo.translatesAutoresizingMaskIntoConstraints = false
    let heightConstraint = NSLayoutConstraint(item: cryptoLogo, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)
    let widthConstraint = NSLayoutConstraint(item: cryptoLogo, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)

    lblCryptoPrice.translatesAutoresizingMaskIntoConstraints = false
    let lblHeightConstraint = NSLayoutConstraint(item: lblCryptoPrice, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 35)

    lblCryptoAccount.translatesAutoresizingMaskIntoConstraints = false
    let lblCryptoAccountHeight = NSLayoutConstraint(item: lblCryptoAccount, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 35)

    lblCryptoAccountConverted.translatesAutoresizingMaskIntoConstraints = false
    let lblCryptoAccountConvertedHeight = NSLayoutConstraint(item: lblCryptoAccountConverted, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 35)

    NSLayoutConstraint.activate([heightConstraint, widthConstraint, lblHeightConstraint, lblCryptoAccountHeight, lblCryptoAccountConvertedHeight])

    let stackCryptoData = UIStackView(arrangedSubviews: [cryptoLogo, lblCryptoPrice, lblCryptoAccount, lblCryptoAccountConverted])
    stackCryptoData.alignment = .center
    stackCryptoData.axis = .vertical
    stackCryptoData.distribution = .equalCentering
    view.addSubview(stackCryptoData)

    stackCryptoData.translatesAutoresizingMaskIntoConstraints = false
    let leadingSpace = NSLayoutConstraint(item: stackCryptoData, attribute: .leadingMargin, relatedBy: .equal, toItem: view, attribute: .leadingMargin, multiplier: 1.0, constant: 20)
    let trailingSpace = NSLayoutConstraint(item: stackCryptoData, attribute: .trailingMargin, relatedBy: .equal, toItem: view, attribute: .trailingMargin, multiplier: 1.0, constant: -20)
    let topSpace = NSLayoutConstraint(item: stackCryptoData, attribute: .topMargin, relatedBy: .equal, toItem: chart, attribute: .bottom, multiplier: 1.0, constant: 20)
    let bottomSpace = NSLayoutConstraint(item: stackCryptoData, attribute: .bottomMargin, relatedBy: .equal, toItem: view, attribute: .bottomMargin, multiplier: 1.0, constant: -20)
    NSLayoutConstraint.activate([leadingSpace, trailingSpace, topSpace, bottomSpace])
  }

  private func loadData() {
    guard let currency = currency else { return }
    cryptoLogo.image = currency.imgLogo
    loadCryptoPrice(of: currency)
    currency.getHistoricalData(in: currencyType.rawValue)
  }

  private func loadCryptoPrice(of currency: Currency) {
    let currencyPrice = currency.price[currencyType.rawValue] ?? 0.0
    lblCryptoAccount.text = "You Own: \(currency.amount) \(currency.symbol)"
    switch currencyType {
    case .USD:
      lblCryptoPrice.text = currencyPrice.usdCurrency
      lblCryptoAccountConverted.text = (currencyPrice * currency.amount).usdCurrency
    case .EUR:
      lblCryptoPrice.text = currencyPrice.eurCurrency
      lblCryptoAccountConverted.text = (currencyPrice * currency.amount).eurCurrency
    case .MXN:
      lblCryptoPrice.text = currencyPrice.mxnCurrency
      lblCryptoAccountConverted.text = (currencyPrice * currency.amount).mxnCurrency
    }
  }

  // MARK: - Selectors

  @objc private func segmentDidChangeValue(_ sender: UISegmentedControl) {
    switch sender.selectedSegmentIndex {
    case 0:
      currencyType = .USD
    case 1:
      currencyType = .EUR
    case 2:
      currencyType = .MXN
    default:
      currencyType = .USD
    }

    loadData()
  }

  @objc private func editTapped(_ sender: UIBarButtonItem) {
    guard let currency = currency else { return }
    let alert = UIAlertController(title: "How much \(currency.symbol)", message: nil, preferredStyle: .alert)
    alert.addTextField(configurationHandler: { textfield in
      textfield.placeholder = "0.5"
      textfield.keyboardType = .decimalPad
      textfield.text = currency.amount != 0.0 ? "\(currency.amount)" : nil
    })
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
      guard let text = alert.textFields?.first?.text, let amount = Double(text) else { return }
      currency.amount = amount
      userDefaults.set(amount, forKey: currency.symbol + "_amount_key")
      self.newPrices()
    }))
    present(alert, animated: true, completion: nil)
  }

}

extension CurrencyVC: CryptoDataDelegate {

  func newHistory() {
    guard let currency = currency else { return }
    chart.removeAllSeries()
    let series = ChartSeries(currency.historicalData)
    series.area = true
    chart.add(series)
  }

  func newPrices() {
    guard let currency = currency else { return }
    loadCryptoPrice(of: currency)
  }
}
