//
//  AuthenticationVC.swift
//  crypto-tracker
//
//  Created by Juan Francisco Dorado Torres on 5/27/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import LocalAuthentication

class AuthenticationVC: UIViewController {

  // MARK: - View Cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.black
    presentAuthentication()
  }

  // MARK: - Functions

  func presentAuthentication() {
    LAContext().evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Your Crypto is protected by biometrics") { (success, error) in
      DispatchQueue.main.async {
        if success {
          let cryptoVC = CryptoVC()
          let navController = UINavigationController(rootViewController: cryptoVC)
          self.present(navController, animated: true, completion: nil)
        } else {
          self.presentAuthentication()
        }
      }
    }
  }
}
