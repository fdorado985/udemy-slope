//
//  CryptoData.swift
//  crypto-tracker
//
//  Created by Juan Francisco Dorado Torres on 5/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol CryptoDataDelegate : class {

  @objc optional func newPrices()
  @objc optional func newHistory()
}

class CryptoData {

  // MARK: - Singleton

  static let shared = CryptoData()

  // MARK: - Constants

  let cryptoSymbols = ["ABT", "ACT", "ADA", "ADX", "BIX", "BLOCK", "BPT", "BQ", "BRD", "BTC", "ETH", "LTC"]
  let currencyValues = ["USD", "EUR", "MXN"]

  // MARK: - Properties

  let CRYPTO_VALUE_API = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=%@&tsyms=%@"
  var currencies = [Currency]()
  weak var delegate: CryptoDataDelegate?

  // MARK: - Initialization

  private init() {
    cryptoSymbols.forEach { (symbol) in
      let currency = Currency(symbol: symbol)
      currencies.append(currency)
    }
  }

  // MARK: - Functions

  func getPrices() {
    let url = String(format: CRYPTO_VALUE_API, arguments: [cryptoSymbols.joined(separator: ","), currencyValues.joined(separator: ",")])
    Alamofire.request(url).responseJSON { (response) in
      if let json = response.result.value as? [String : Any] {
        self.currencies.forEach({ currency in
          if let currencyJson = json[currency.symbol] as? [String : Double] {
            currency.price = currencyJson
            userDefaults.set(currencyJson, forKey: currency.symbol + "_key")
          }
        })
        self.delegate?.newPrices?()
      }
    }
  }

  func netWorthAsString(for currencyType: CurrencyType) -> String {
    var netWorth = 0.0
    currencies.forEach({ netWorth += $0.amount * ($0.price[currencyType.rawValue] ?? 0.0) })

    switch currencyType {
    case .USD:
      return netWorth.usdCurrency
    case .EUR:
      return netWorth.eurCurrency
    case .MXN:
      return netWorth.mxnCurrency
    }
  }

  func html(for currencyType: CurrencyType) -> String {
    var html = "<h1>MY Crypto Report</h1>"
    html += "<h2>Net Worth (\(currencyType.rawValue)): \(netWorthAsString(for: currencyType))</h2>"
    html += "<ul>"
    currencies.forEach({
      if $0.amount != 0.0 {
        var valuedCurrency = ""
        switch currencyType {
        case .USD:
          valuedCurrency = ($0.amount * ($0.price[currencyType.rawValue] ?? 0.0)).usdCurrency
        case .EUR:
          valuedCurrency = ($0.amount * ($0.price[currencyType.rawValue] ?? 0.0)).eurCurrency
        case .MXN:
          valuedCurrency = ($0.amount * ($0.price[currencyType.rawValue] ?? 0.0)).mxnCurrency
        }

        html += "<li>\($0.symbol) - I own: \($0.amount) - Valued at: \(currencyType.rawValue): \(valuedCurrency)</li>"
      }
    })
    html += "</ul>"

    return html
  }
}
