//
//  CryptocurrencyCell.swift
//  crypto-tracker
//
//  Created by Juan Francisco Dorado Torres on 5/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class CryptocurrencyCell: UITableViewCell {

  // MARK: - IBOutlets

  @IBOutlet weak var imgLogo: UIImageView!
  @IBOutlet weak var txtSymbol: UILabel!
  @IBOutlet weak var txtUSDValue: UILabel!
  @IBOutlet weak var txtEURValue: UILabel!
  @IBOutlet weak var txtMXNValue: UILabel!
  @IBOutlet weak var txtAmount: UILabel!
  @IBOutlet weak var stackOwn: UIStackView!

  // MARK: - Configure Cell

  func configureCell(currency: Currency) {
    imgLogo.image = currency.imgLogo
    txtSymbol.text = currency.symbol

    // Prices
    let usdValue = currency.price["USD"] ?? 0.00
    let eurValue = currency.price["EUR"] ?? 0.00
    let mxnValue = currency.price["MXN"] ?? 0.00

    txtUSDValue.text = "USD: \(usdValue.usdCurrency)"
    txtEURValue.text = "EUR: \(eurValue.eurCurrency)"
    txtMXNValue.text = "MXN: \(mxnValue.mxnCurrency)"

    txtAmount.text = "\(currency.amount)"
    stackOwn.isHidden = currency.amount == 0.0
  }
}
