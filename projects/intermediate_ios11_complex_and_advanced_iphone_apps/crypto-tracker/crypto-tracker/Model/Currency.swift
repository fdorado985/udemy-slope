//
//  Currency.swift
//  crypto-tracker
//
//  Created by Juan Francisco Dorado Torres on 5/26/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit
import Alamofire

// MARK: - Enum

enum CurrencyType: String {
  case USD = "USD"
  case EUR = "EUR"
  case MXN = "MXN"
}

class Currency {

  public private(set) var symbol = ""
  public private(set) var imgLogo = UIImage()
  public var price = [String : Double]()
  public var amount = 0.0
  public private(set) var historicalData = [Double]()

  init(symbol: String) {
    self.symbol = symbol
    self.imgLogo = UIImage(named: symbol.lowercased()) ?? UIImage()
    getSavedData()
  }

  private func getSavedData() {
    if let prices = userDefaults.dictionary(forKey: symbol + "_key") as? [String : Double] {
      self.price = prices
    }

    if let history = userDefaults.array(forKey: symbol + "_history_key") as? [Double] {
      self.historicalData = history
    }

    self.amount = userDefaults.double(forKey: symbol + "_amount_key")
  }

  func getHistoricalData(in currencyValue: String) {
    let CRYPTO_HISTORICAL_API = "https://min-api.cryptocompare.com/data/histoday?fsym=\(symbol)&tsym=\(currencyValue)&limit=30"
    Alamofire.request(CRYPTO_HISTORICAL_API).responseJSON { (response) in
      if let json = response.result.value as? [String : Any], let data = json["Data"] as? [[String : Double]] {
        self.historicalData = []
        for element in data {
          if let closePrice = element["close"] {
            self.historicalData.append(closePrice)
          }
        }
        CryptoData.shared.delegate?.newHistory?()
        userDefaults.set(self.historicalData, forKey: self.symbol + "_history_key")
      }
    }
  }
}
