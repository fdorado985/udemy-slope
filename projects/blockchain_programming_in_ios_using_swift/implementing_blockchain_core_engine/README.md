# Implementing Blockchain Core Engine
Here you are gonna be able to see a `playground` (`macOS playground`) to see how is the behaviour of `Blockchain` inside code.

Once you understand this, you can be able to take this logic and put it on your `API`.

I cannot use a demo in here, because is a code example that needs to print inside `console`.

So for this you are gonna need

* [Xcode](https://developer.apple.com/xcode/) - This playground was created using `Xcode 10.0`

And that's all.

## Running Debug
![blockchain_hash_demo](.screenshots/blockchain_hash_demo.gif)
