import Cocoa


// MARK: - Extensions

extension String {
    
    func sha1Hash() -> String {
        let task = Process()
        task.launchPath = "/usr/bin/shasum"
        task.arguments = []
        
        let inputPipe = Pipe()
        inputPipe.fileHandleForWriting.write(self.data(using: String.Encoding.utf8)!)
        inputPipe.fileHandleForWriting.closeFile()
        
        let outputPipe = Pipe()
        task.standardOutput = outputPipe
        task.standardInput = inputPipe
        task.launch()
        
        let data = outputPipe.fileHandleForReading.readDataToEndOfFile()
        let hash = String(data: data, encoding: String.Encoding.utf8)!
        return hash.replacingOccurrences(of: " -\n", with: "")
    }
}

// MARK: Blockchain Engine


/**
 This will be working as a transaction inside a Block
 */
class Transaction: Codable {
    
    // MARK: Properties
    
    var from: String
    var to: String
    var amount: Double
    
    // MARK: Initialization
    
    init(from: String, to: String, amount: Double) {
        self.from = from
        self.to = to
        self.amount = amount
    }
}

/**
 This will be working as a block inside a Blockchain that is gonna be able to save a lot transactions
 */
class Block {
    
    // MARK: Properties
    
    var index: Int = 0
    var previousHash = ""
    var hash: String!
    var nonce: Int
    
    var key: String {
        get {
            do {
                let transactionsData = try JSONEncoder().encode(self.transactions)
                guard let transactionsJSONString = String(data: transactionsData, encoding: .utf8) else { fatalError("\(#function): transactionsData couldn't be converted") }
                
                return String(self.index) + self.previousHash + String(self.nonce) + transactionsJSONString
            } catch let error {
                fatalError("\(#function): \(error.localizedDescription)")
            }
            
        }
    }
    
    private(set) var transactions = [Transaction]()
    
    // MARK: Initialization
    
    init() {
        self.nonce = 0
    }
    
    // MARK: Functions
    
    func addTransaction(transaction: Transaction) {
        self.transactions.append(transaction)
    }
}

/**
 This is the full engine... will save our blocks that stores transactions on each of them.
 */
class Blockchain {
    
    // MARK: Properties
    
    private(set) var blocks = [Block]()
    
    // MARK: Initialization
    
    init(genesisBlock: Block) {
        addBlock(genesisBlock)
    }
    
    // MARK: Functions
    
    func addBlock(_ block: Block) {
        if self.blocks.isEmpty {
            block.previousHash = "0000000000000000"
            block.hash = generateHash(for: block)
        }
        
        self.blocks.append(block)
    }
    
    func getNextBlock(transactions: [Transaction]) -> Block {
        let block = Block()
        transactions.forEach { block.addTransaction(transaction: $0) }
        let previousBlock = getPreviousBlock()
        block.index = self.blocks.count
        block.previousHash = previousBlock.hash
        block.hash = generateHash(for: block)
        return block
    }
    
    private func getPreviousBlock() -> Block {
        guard let previousBlock = self.blocks.last else {
            fatalError("\(#function): Couldn't get previous block")
        }
        
        return previousBlock
    }
    
    private func generateHash(for block: Block) -> String {
        var hash = block.key.sha1Hash()
        
        while !hash.hasPrefix("00") {
            block.nonce += 1
            hash = block.key.sha1Hash()
            debugPrint(hash)
        }
        
        return hash
    }
}


// TEST BLOCKCHAIN

// The Blockchain requires an initial block called "Genesis Block", so at this time... the Blockchain will be generating hash until it finds one that starts with "00..."
let genesisBlock = Block()
let blockchain = Blockchain(genesisBlock: genesisBlock)

debugPrint("----------------------------------------------------------------------")

// After here we create a new transaction and add it to our blockchain
let transaction = Transaction(from: "Mary", to: "Steve", amount: 10)
let block = blockchain.getNextBlock(transactions: [transaction])
blockchain.addBlock(block)

debugPrint(blockchain.blocks.count) // This is to see how many block do we have on our blockchain
