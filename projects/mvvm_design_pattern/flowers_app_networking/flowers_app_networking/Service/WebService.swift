//
//  WebService.swift
//  flowers_app_networking
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

private var url = URL(string: "https://swapi.co/api/films/")

typealias JSONDictionary = [String : Any]

class WebService {
    
    func loadFilms(handler: @escaping (([Film]) -> ())) {
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []),
                let dict = json as? JSONDictionary,
                let results = dict["results"] as? [JSONDictionary] {
                var films = [Film]()
                
                for result in results {
                    let film = Film(dictionary: result)
                    films.append(film)
                }
                
                DispatchQueue.main.async {
                    handler(films)
                }
            }
        }.resume()
    }
}
