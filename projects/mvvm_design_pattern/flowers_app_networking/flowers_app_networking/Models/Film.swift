//
//  Film.swift
//  flowers_app_networking
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Film {
    
    // MARK: - Properties
    
    var title: String
    var opening_crawl: String
    var director: String
    var producer: String
    var releaseDate: String
    
    // MARK: - Initialization
    
    init(dictionary: JSONDictionary) {
        guard let title = dictionary["title"] as? String,
            let openingCrawl = dictionary["opening_crawl"] as? String,
            let director = dictionary["director"] as? String,
            let producer = dictionary["producer"] as? String,
            let releaseDate = dictionary["release_date"] as? String else {
                self.title = ""
                self.opening_crawl = ""
                self.director = ""
                self.producer = ""
                self.releaseDate = ""
                return
        }
        
        self.title = title
        self.opening_crawl = openingCrawl
        self.director = director
        self.producer = producer
        self.releaseDate = releaseDate
    }
    
}
