//
//  FilmsListViewModel.swift
//  flowers_app_networking
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class FilmsListViewModel {
    
    // MARK: - Properties
    
    var filmViewModels = [FilmViewModel]()
    
    // MARK: - Initialization
    
    init(_ films: [Film]) {
        self.filmViewModels = films.map(FilmViewModel.init)
    }
    
    init(webservice: WebService, handler: @escaping () -> ()) {
        webservice.loadFilms { (films) in
            self.filmViewModels = films.map(FilmViewModel.init)
            DispatchQueue.main.async {
                handler()
            }
        }
    }
}
