//
//  FilmViewModel.swift
//  flowers_app_networking
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class FilmViewModel {
    
    var title: String
    var opening_crawl: String
    var director: String
    var producer: String
    var releaseDate: String
    
    init(film: Film) {
        self.title = film.title
        self.opening_crawl = film.opening_crawl
        self.director = film.director
        self.producer = film.producer
        self.releaseDate = film.releaseDate
    }
}
