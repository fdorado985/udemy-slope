//
//  FilmsVC.swift
//  flowers_app_networking
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class FilmsVC: UITableViewController {
    
    // MARK: - Properties
    
    private var filmsListViewModel: FilmsListViewModel?
    private var webservice = WebService()
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Here is a web service out of viewmodel
        /*WebService().loadFilms { (films) in
            self.filmsListViewModel = FilmsListViewModel(films)
            self.tableView.reloadData()
        }*/
        
        // Here is a web service call inside viewmodel
        self.filmsListViewModel = FilmsListViewModel(webservice: self.webservice, handler: {
            self.tableView.reloadData()
        })
    }
}

// MARK: - UITableView Delegate|DataSource

extension FilmsVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let filmsListViewModel = filmsListViewModel else { return 0 }
        return filmsListViewModel.filmViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilmCell", for: indexPath)
        
        if let filmsListViewModel = self.filmsListViewModel {
            let film = filmsListViewModel.filmViewModels[indexPath.row]
            cell.textLabel?.text = film.title
            cell.detailTextLabel?.text = film.opening_crawl
        }
        
        return cell
    }
}
