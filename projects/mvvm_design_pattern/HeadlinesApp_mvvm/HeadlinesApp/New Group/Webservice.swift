//
//  Webservice.swift
//  HeadlinesApp
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import Foundation

class Webservice {
    
    // MARK: - Properties
    
    // MARK: - Functions
    
    func loadSources(handler: @escaping ([Source]) -> ()) {
        var sources = [Source]()
        let sourceURL = URL(string :"https://newsapi.org/v2/sources?apiKey=0cf790498275413a9247f8b94b3843fd")!
        
        URLSession.shared.dataTask(with: sourceURL) { data, _, _ in
            if let data = data {
                let json = try! JSONSerialization.jsonObject(with: data, options: [])
                let dictionary = json as! [String:Any]
                let sourcesDictionary = dictionary["sources"] as! [[String:Any]]
                
                sources = sourcesDictionary.compactMap(Source.init)
                
                DispatchQueue.main.async {
                    handler(sources)
                }
            }
        }.resume()
    }
    
    func loadHeadlinesForSource(source: Source, handler: @escaping ([Headline]) -> ()) {
        var headlines = [Headline]()
        let url = URL(string :"https://newsapi.org/v2/top-headlines?sources=\(source.id)&apiKey=0cf790498275413a9247f8b94b3843fd")!
        
        URLSession.shared.dataTask(with: url) { data, _, _ in
            if let data = data {
                let json = try! JSONSerialization.jsonObject(with: data, options: [])
                let dictionary = json as! [String:Any]
                let headlineDictionaries = dictionary["articles"] as! [[String:Any]]
                
                headlines = headlineDictionaries.compactMap(Headline.init)
                
                DispatchQueue.main.async {
                    handler(headlines)
                }
            }
        }.resume()
    }
}
