//
//  TableViewDataSource.swift
//  HeadlinesApp
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import UIKit

class TableViewDataSource<Cell: UITableViewCell, ViewModel>: NSObject, UITableViewDataSource {
    
    // MARK: - Properties
    
    private var cellIdentifier: String
    private var items: [ViewModel]
    var configureCell: (Cell, ViewModel) -> ()
    
    // MARK: - Initialization
    
    init(cellIdentifier: String, items: [ViewModel], configureCell: @escaping (Cell, ViewModel) -> ()) {
        self.cellIdentifier = cellIdentifier
        self.items = items
        self.configureCell = configureCell
    }
    
    // MARK: - TableView Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? Cell else { fatalError("Unable to reach Cell<T>") }
        
        let item = self.items[indexPath.row]
        self.configureCell(cell, item)
        
        return cell
    }
}
