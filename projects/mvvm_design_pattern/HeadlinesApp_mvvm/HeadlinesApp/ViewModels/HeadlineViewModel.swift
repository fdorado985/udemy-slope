//
//  HeadlineViewModel.swift
//  HeadlinesApp
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import Foundation

class HeadlineViewModel {
    
    // MARK: - Properties
    
    var title: String
    var description: String
    
    // MARK: - Initialization
    
    init(title: String, description: String) {
        self.title = title
        self.description = description
    }
    
    init(headline: Headline) {
        self.title = headline.title
        self.description = headline.description
    }
}
