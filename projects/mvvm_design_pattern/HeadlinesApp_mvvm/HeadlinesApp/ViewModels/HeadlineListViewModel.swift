//
//  HeadLineListViewModel.swift
//  HeadlinesApp
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import Foundation

class HeadlineListViewModel {
    
    // MARK: - Properties
    
    private(set) var headlineViewModels = [HeadlineViewModel]()
    
    // MARK: - Initialization
    
    init(sourceViewModel: SourceViewModel, handler: @escaping () -> ()) {
        let source = Source(sourceViewModel: sourceViewModel)
        Webservice().loadHeadlinesForSource(source: source) { (headlines) in
            self.headlineViewModels = headlines.map(HeadlineViewModel.init)
            
            DispatchQueue.main.async {
                handler()
            }
        }
    }
}
