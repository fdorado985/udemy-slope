//
//  SourceListViewModel.swift
//  HeadlinesApp
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import Foundation

class SourceListViewModel {
    
    // MARK: - Properties
    
    private(set) var sourcesViewModel = [SourceViewModel]()
    private var webservice: Webservice
    private var completion: () -> () = { }
    
    // MARK: - Initialization
    
    init(webservice: Webservice, handler: @escaping () -> ()) {
        self.webservice = webservice
        self.completion = handler
        populateSources()
    }
    
    // MARK: - Functions
    
    private func populateSources() {
        self.webservice.loadSources { (sources) in
            self.sourcesViewModel = sources.map(SourceViewModel.init)
            self.completion()
        }
    }
    
    func sourceAt(index: Int) -> SourceViewModel {
        return self.sourcesViewModel[index]
    }
}
