//
//  SourceViewModel.swift
//  HeadlinesApp
//
//  Created by Juan Francisco Dorado Torres on 7/29/18.
//  Copyright © 2018 Mohammad Azam. All rights reserved.
//

import Foundation

class SourceViewModel {
    
    var id: String
    var name: String
    var description: String
    
    init(id: String, name: String, description: String) {
        self.id = id
        self.name = name
        self.description = description
    }
    
    init(source: Source) {
        self.id = source.id
        self.name = source.name
        self.description = source.description
    }
}
