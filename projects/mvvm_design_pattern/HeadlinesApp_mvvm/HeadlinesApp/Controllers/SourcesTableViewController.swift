//
//  SourcesTableViewController.swift
//  HeadlinesApp
//
//  Created by Mohammad Azam on 12/8/17.
//  Copyright © 2017 Mohammad Azam. All rights reserved.
//

import Foundation
import UIKit

class SourcesTableViewController : UITableViewController {
    
    // MARK: - Properties
    
    private var webservice: Webservice?
    private var sourceListViewModel: SourceListViewModel?
    private var dataSource: TableViewDataSource<SourceTableViewCell, SourceViewModel>?
    
    // MARK: - Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webservice = Webservice()
        self.sourceListViewModel = SourceListViewModel(webservice: self.webservice ?? Webservice(), handler: {
            guard let sourceListViewModel = self.sourceListViewModel else { return }
            self.dataSource = TableViewDataSource(cellIdentifier: "Cell", items: sourceListViewModel.sourcesViewModel, configureCell: { (cell, viewModel) in
                cell.titleLabel.text = viewModel.name
                cell.descriptionLabel.text = viewModel.description
            })
            
            self.tableView.dataSource = self.dataSource
            self.tableView.reloadData()
        })
        
    }
    
    // MARK: - Functions
    
    private func populateSources() {
        guard let webservice = self.webservice else { return }
        webservice.loadSources { (sources) in
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let headlinesTVC = segue.destination as? HeadlinesTableViewController else {
            fatalError("HeadlinesTableViewController not found")
        }
        
        guard let indexPath = (self.tableView.indexPathForSelectedRow),
            let sourceListViewModel = self.sourceListViewModel else { return }
        
        let source = sourceListViewModel.sourceAt(index: indexPath.row)
        headlinesTVC.sourceViewModel = source
        
    }
}

// MARK: - UITableView Delegate|Datasource (use instead of dataSource property)
/*
extension SourcesTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sourceListViewModel = self.sourceListViewModel else { return 0 }
        return sourceListViewModel.sourcesViewModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? SourceTableViewCell else { return UITableViewCell() }
        
        if let sourceListViewModel = self.sourceListViewModel {
            let source = sourceListViewModel.sourceAt(index: indexPath.row)
            cell.titleLabel.text = source.name
            cell.descriptionLabel.text = source.description
        }
        
        return cell
    }
}*/








