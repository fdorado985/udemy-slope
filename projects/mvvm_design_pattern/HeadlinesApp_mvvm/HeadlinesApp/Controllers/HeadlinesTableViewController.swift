//
//  HeadlinesTableViewController.swift
//  HeadlinesApp
//
//  Created by Mohammad Azam on 12/16/17.
//  Copyright © 2017 Mohammad Azam. All rights reserved.
//

import Foundation
import UIKit

class HeadlinesTableViewController : UITableViewController {
    
    // MARK: - Properties
    
    var sourceViewModel: SourceViewModel?
    private var headlineListViewModel: HeadlineListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
    }
    
    private func updateUI() {
        guard let sourceViewModel = self.sourceViewModel else { return }
        
        self.title = sourceViewModel.name
        self.headlineListViewModel = HeadlineListViewModel(sourceViewModel: sourceViewModel, handler: {
            self.tableView.reloadData()
        })
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let headlineListViewModel = self.headlineListViewModel else { return 0 }
        return headlineListViewModel.headlineViewModels.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? HeadlineTableViewCell else {
            fatalError("Unable to look for HeadlineTableViewCell")
        }
        
        if let headlineListViewModel = self.headlineListViewModel {
            let headline = headlineListViewModel.headlineViewModels[indexPath.row]
            cell.titleLabel.text = headline.title
            cell.descriptionLabel.text = headline.description
        }
        
        return cell
    }
    
}
