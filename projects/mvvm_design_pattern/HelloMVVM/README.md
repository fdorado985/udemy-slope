#  Hello MVVM

Simple app that shows how MVVM works.
Here we will see how to divide `Model View ViewModel` in a project... even how it works as a full scene and sub-scenes.

## Demo
![hello_mvvm_demo](screenshots/hellomvvm_demo.gif)

