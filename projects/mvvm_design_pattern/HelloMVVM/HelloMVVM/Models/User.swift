//
//  User.swift
//  HelloMVVM
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class User {
    
    // MARK: Properties
    
    var firstName: String
    var lastName: String
    var email: String
    var password: String
    
    // MARK: init by viewModel
    
    init(viewModel: RegistrationViewModel) {
        self.firstName = viewModel.firstName
        self.lastName = viewModel.lastName
        self.email = viewModel.email
        self.password = viewModel.password
    }
    
    // MARK: init by properties
    
    init(firstName: String, lastName: String, email: String, password: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.password = password
    }
}
