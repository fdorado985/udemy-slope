//
//  DataAccess.swift
//  HelloMVVM
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class DataAccess {
    
    // MARK: - Functions
    
    func getAllUsers() -> [User] {
        var users = [User]()
        users.append(User(firstName: "Juan", lastName: "Dorado", email: "juan.dorado@mailinator.com", password: "password"))
        users.append(User(firstName: "John", lastName: "Doe", email: "john.doe@mailinator.com", password: "password"))
        users.append(User(firstName: "Mary", lastName: "Kate", email: "mary.kate@mailinator.com", password: "password"))
        users.append(User(firstName: "Alex", lastName: "Lowe", email: "alex.lowe@mailinator.com", password: "password"))
        
        return users
    }
}
