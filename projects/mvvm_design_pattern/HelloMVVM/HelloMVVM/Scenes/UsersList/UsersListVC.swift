//
//  UsersListVC.swift
//  HelloMVVM
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class UsersListVC: UITableViewController {
    
    // MARK: - Properties
    
    private var usersListViewModel: UsersListViewModel?
    private var dataAccess: DataAccess?
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataAccess = DataAccess()
        guard let dataAccess = dataAccess else { return }
        self.usersListViewModel = UsersListViewModel(dataAccess: dataAccess)
        
        self.tableView.reloadData()
    }
    
    // MARK - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RegistrationVC",
            let registrationVC = segue.destination as? RegistrationVC,
            let indexPath = self.tableView.indexPathForSelectedRow,
            let usersListViewModel = self.usersListViewModel {
            let selectedUserViewModel = usersListViewModel.usersViewModel[indexPath.row]
            registrationVC.userViewModel = selectedUserViewModel
        }
    }

    
}

// MARK: - UITableView Datasource

extension UsersListVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let usersListViewModel = usersListViewModel else { return 0 }
        return usersListViewModel.usersViewModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath)
        
        if let usersListViewModel = self.usersListViewModel {
            let userViewModel = usersListViewModel.usersViewModel[indexPath.row]
            cell.textLabel?.text = "\(userViewModel.firstName) \(userViewModel.lastName)"
            cell.detailTextLabel?.text = userViewModel.email
        }
        
        return cell
    }
}
