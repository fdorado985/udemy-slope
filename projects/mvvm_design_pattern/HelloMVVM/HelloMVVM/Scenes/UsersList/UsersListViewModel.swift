//
//  UserListViewModel.swift
//  HelloMVVM
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

// This will represents the whole list of users
class UsersListViewModel {
    
    // MARK: - Properties
    
    private var dataAccess: DataAccess
    
    var usersViewModel: [UserViewModel] = []
    
    // MARK: - init by dataAccess
    
    init(dataAccess: DataAccess) {
        self.dataAccess = dataAccess
        populateUsers()
    }
    
    // MARK: - Functions
    
    private func populateUsers() {
        let users = self.dataAccess.getAllUsers()
        self.usersViewModel = users.map({ UserViewModel(user: $0) })
    }
    
}

// This will represents every single user to be handle for the UsersListViewModel
class UserViewModel {
    
    // MARK - Properties
    
    var firstName: String
    var lastName: String
    var email: String
    
    init(user: User) {
        self.firstName = user.firstName
        self.lastName = user.lastName
        self.email = user.email
    }
}
