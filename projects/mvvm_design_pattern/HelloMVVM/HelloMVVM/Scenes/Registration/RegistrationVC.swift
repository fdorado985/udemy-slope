//
//  RegistrationVC.swift
//  HelloMVVM
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RegistrationVC: UITableViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    // MARK: - Properties
    
    private var registrationViewModel: RegistrationViewModel?
    
    var userViewModel: UserViewModel?

    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK: - IBActions
    
    @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
        self.registrationViewModel = RegistrationViewModel(firstName: txtFirstName.text ?? "",
                                                           lastName: txtLastName.text ?? "",
                                                           email: txtEmail.text ?? "",
                                                           password: txtPassword.text ?? "")
        self.registrationViewModel?.save()
    }
    
    // MARK: - Functions
    
    public func setupView() {
        guard let userViewModel = self.userViewModel else { return }
        txtFirstName.text = userViewModel.firstName
        txtLastName.text = userViewModel.lastName
        txtEmail.text = userViewModel.email
    }
}

