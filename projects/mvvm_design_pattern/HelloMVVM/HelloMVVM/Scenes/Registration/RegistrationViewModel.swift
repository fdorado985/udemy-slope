//
//  RegistrationViewModel.swift
//  HelloMVVM
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class RegistrationViewModel {
    
    // MARK: - Properties
    
    var firstName: String
    var lastName: String
    var email: String
    var password: String
    
    // MARK: - init()
    
    init(firstName: String, lastName: String, email: String, password: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.password = password
    }
    
    // MARK: - Public
    
    func save() {
        let user = User(viewModel: self)
    }
}
