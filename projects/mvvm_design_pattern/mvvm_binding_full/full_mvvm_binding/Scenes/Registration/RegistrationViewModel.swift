//
//  RegistrationViewModel.swift
//  full_mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Dynamic<T> {
    
    // MARK: - Properties
    
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T? {
        didSet {
            if let value = self.value {
                listener?(value)
            }
        }
    }
    
    // MARK: - Init
    
    init(_ value: T) {
        self.value = value
    }
    
    // MARK: - Functions
    
    func bind(listener: Listener?) {
        guard let value = self.value else { return }
        self.listener = listener
        listener?(value)
    }
}

class RegistrationViewModel {
    
    // MARK: - Properties
    
    var email: String
    var password: String
    
    // MARK: - Initialization
    
    init() {
        self.email = ""
        self.password = ""
    }
}
