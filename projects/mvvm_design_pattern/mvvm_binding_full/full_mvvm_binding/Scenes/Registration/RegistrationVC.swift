//
//  RegistrationVC.swift
//  full_mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RegistrationVC: UITableViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var txtEmail: BindingTextField! {
        didSet {
            txtEmail.bind { self.registrationViewModel.email = $0 }
        }
    }
    @IBOutlet weak var txtPassword: BindingTextField! {
        didSet {
            txtPassword.bind { self.registrationViewModel.password = $0 }
        }
    }
    
    // MARK: - Properties
    
    private var registrationViewModel: RegistrationViewModel!
    
    var user: UserViewModel?
    
    // MARK: - View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registrationViewModel = RegistrationViewModel()
        setupView()
    }
    
    // MARK: - IBActions
    
    @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
        print(self.registrationViewModel)
        
        guard let user = self.user else { return }
        user.email.value = "Mary"
        user.password.value = "mary_password"
    }
    
    // MARK: - Functions
    
    func setupView() {
        guard let user = self.user else { return }
        user.email.bind { self.txtEmail.text = $0 }
        user.password.bind { self.txtPassword.text = $0 }
    }
}
