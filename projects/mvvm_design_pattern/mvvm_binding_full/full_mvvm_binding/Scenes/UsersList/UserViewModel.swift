//
//  UserViewModel.swift
//  full_mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class UserViewModel {
    
    // MARK: - Properties
    
    var email: Dynamic<String>
    var password: Dynamic<String>
    
    // MARK: - Initialization
    
    init(email: String, password: String) {
        self.email = Dynamic<String>(email)
        self.password = Dynamic<String>(password)
    }
}
