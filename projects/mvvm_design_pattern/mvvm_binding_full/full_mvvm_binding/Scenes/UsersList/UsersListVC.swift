//
//  UsersListVC.swift
//  full_mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class UsersListVC: UITableViewController {
    
    // MARK: - Properties
    
    private var users = [UserViewModel]()
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user1 = UserViewModel(email: "johndoe@mailinator.com", password: "password")
        self.users.append(user1)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "RegistrationVC",
            let registrationVC = segue.destination as? RegistrationVC,
            let indexPath = self.tableView.indexPathForSelectedRow else { return }
        
        let user = self.users[indexPath.row]
        registrationVC.user = user
    }
}

// MARK: - UITableView Delegate|Datasource

extension UsersListVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath)
        
        let user = self.users[indexPath.row]
        cell.textLabel?.text = user.email.value
        
        return cell
    }
}
