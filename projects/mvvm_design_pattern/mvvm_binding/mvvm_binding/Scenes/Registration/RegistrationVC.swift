//
//  RegistrationVC.swift
//  mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class RegistrationVC: UITableViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var txtEmail: BindingTextField! {
        didSet {
            txtEmail.bind { self.registrationViewModel.email = $0 }
        }
    }
    @IBOutlet weak var txtPassword: BindingTextField! {
        didSet {
            txtPassword.bind { self.registrationViewModel.password = $0 }
        }
    }
    
    // MARK: - Properties
    
    private var registrationViewModel: RegistrationViewModel!
    
    // MARK: - View cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        registrationViewModel = RegistrationViewModel()
    }
    
    // MARK: - IBActions
    
    @IBAction func btnSaveTapped(_ sender: UIBarButtonItem) {
        print(self.registrationViewModel)
    }
    
}
