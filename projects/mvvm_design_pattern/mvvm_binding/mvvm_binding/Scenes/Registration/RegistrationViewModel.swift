//
//  RegistrationViewModel.swift
//  mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class RegistrationViewModel {
    
    // MARK: - Properties
    
    var email: String
    var password: String
    
    // MARK: - Initialization
    
    init() {
        self.email = ""
        self.password = ""
    }
}
