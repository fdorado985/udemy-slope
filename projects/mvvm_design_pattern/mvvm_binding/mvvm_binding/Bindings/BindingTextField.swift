//
//  BindingTextField.swift
//  mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import UIKit

class BindingTextField: UITextField {
    
    // MARK: - Properties
    
    var textChanged: (String) -> () = { _ in }
    
    // MARK: - Public Functions
    
    func bind(callback: @escaping (String) -> ()) {
        self.textChanged = callback
        self.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.textChanged(textField.text ?? "")
    }
}
