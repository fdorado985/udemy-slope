//
//  RegistrationViewModel.swift
//  full_mvvm_binding
//
//  Created by Juan Francisco Dorado Torres on 7/28/18.
//  Copyright © 2018 Juan Francisco Dorado Torres. All rights reserved.
//

import Foundation

class Dynamic<T> {
    
    // MARK: - Properties
    
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T? {
        didSet {
            if let value = self.value {
                listener?(value)
            }
        }
    }
    
    // MARK: - Init
    
    init(_ value: T) {
        self.value = value
    }
    
    // MARK: - Functions
    
    func bind(listener: Listener?) {
        guard let value = self.value else { return }
        self.listener = listener
        listener?(value)
    }
}

struct BrokenRule {
    
    var propertyName: String
    var message: String
}

protocol ViewModel {
    
    var brokenRules: [BrokenRule] { get set }
    var isValid: Bool { mutating get }
}

class RegistrationViewModel: ViewModel {
    
    var brokenRules: [BrokenRule] = [BrokenRule]()
    
    var isValid: Bool {
        get {
            self.brokenRules = [BrokenRule]()
            self.validate()
            return self.brokenRules.count == 0 ? true : false
        }
    }
    
    // MARK: - Properties
    
    var email: String
    var password: String
    
    // MARK: - Initialization
    
    init() {
        self.email = ""
        self.password = ""
    }
    
    // MARK - Functions
    
    private func validate() {
        if email.isEmpty {
            self.brokenRules.append(BrokenRule(propertyName: "email", message: "Email is required"))
        }
        
        if password.isEmpty {
            self.brokenRules.append(BrokenRule(propertyName: "password", message: "Password is required"))
        }
    }
}
