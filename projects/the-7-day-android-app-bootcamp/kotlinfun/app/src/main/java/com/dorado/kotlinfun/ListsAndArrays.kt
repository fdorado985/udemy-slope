package com.dorado.kotlinfun

fun main(args: Array<String>) {

    // Lists and Arrays

    var topCandy: List<String> = listOf("Fun Dip", "Snickers", "100 Grand Bar")
    var topMovies: Array<String> = arrayOf("Fun Dip", "Snickers", "100 Grand Bar")
    var topMutableMovies: MutableList<String> = mutableListOf("Fun Dip", "Snickers", "100 Grand Bar")
    println(topCandy[2])

    topMovies[0] = "Black Thunder"

    topMutableMovies.add(0, "Thor")
}