package com.dorado.kotlinfun

fun main(args: Array<String>) {

    // If Statements & Booleans

    var age: Int = 29
    var weight: Double = 180.46
    var candy: String = "Snickers"
    var isTheLightOn: Boolean = true
    var canRide: Boolean = age > 11

    if (isTheLightOn) {
        println("Walk across the room")
    } else {
        println("Don't move a muscle")
    }

    if (canRide) {
        println("Enjoy the ride")
    } else {
        println("Sorry son... maybe another day")
    }
}