package com.dorado.kotlinfun

fun main(args: Array<String>) {

    // Loops

    for (x in 1..10_000) {
        println(x)
    }

    var topCandy: MutableList<String> = mutableListOf("Fun Dip", "Snickers", "100 Grand Bar")
    for (x in topCandy) {
        println(x)
    }

    for (x in 0..topCandy.size - 1) {
        topCandy[x] = "##" + topCandy[x] + "##"
        println(topCandy[x])
    }
}