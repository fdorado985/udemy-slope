package com.dorado.kotlinfun

fun main(args: Array<String>) {

    // Functions

    fun printHello() {
        println("Hello World")
    }

    printHello()

    fun addTwoNumbers(number1: Int, number2: Int) {
        val result = number1 + number2
        println(result)
    }

    addTwoNumbers(8, 10)

    fun returnATwoNumbersSum(number1: Int, number2: Int) : Int {
        return number1 + number2
    }

    val sum = returnATwoNumbersSum(10, 10)
    println(sum)
}