package com.dorado.kotlinfun

fun main(args: Array<String>) {
    println("Hello Dad")

    // Variables

    var age = 29
    var candy = "Fun Dip"

    println(age)
    println(candy)

    candy = "Snickers"

    println(candy)

    // Constants

    val name = "Juan"

}