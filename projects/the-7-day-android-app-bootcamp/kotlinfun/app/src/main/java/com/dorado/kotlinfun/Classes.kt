package com.dorado.kotlinfun

fun main(args: Array<String>) {

    // Classes

    class Dog {

        var name = ""
        var age = 0
        var furColor = ""

        fun dogInfo() : String {
            return name + " is " + age + " years old and has " + furColor + " fur"
        }
    }

    var myDog = Dog()
    myDog.age = 9
    myDog.name = "Fido"
    myDog.furColor = "Brown"

    var theOtherDog = Dog()
    theOtherDog.age = 12
    theOtherDog.name = "Sara"
    theOtherDog.furColor = "Black"
    theOtherDog.dogInfo()
}