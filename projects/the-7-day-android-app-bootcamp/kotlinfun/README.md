#  Tip Tip
Learn the basics of the Kotlin language... everything goes just in code separated by class files.

You will see in here:
* DataTypes
* Strings & Ints
* String Interpolations
* Math & Comments
* Conditionals
* List & Array
* Loops
* Functions
* Classes
