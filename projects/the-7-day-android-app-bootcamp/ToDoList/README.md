#  To-Do List
A basic To-Do list app... you will be able to add some task to a list on your app... also... you can mark them as complete... also Delete All of them if you want.

Here you will be able to see:
* Menu Items
* Floating Action Buttons
* Shared Preferences

## Demo
![todo-list-demo](.screenshots/todo-list-demo.gif)
