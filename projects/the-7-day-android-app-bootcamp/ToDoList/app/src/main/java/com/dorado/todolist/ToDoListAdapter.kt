package com.dorado.todolist

import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.todo_item.view.*

class ToDoListAdapter(val todos: List<String>): RecyclerView.Adapter<ToDoListAdapter.ToDoHolder>() {

    override fun onCreateViewHolder(view: ViewGroup, p1: Int): ToDoHolder {
        return ToDoHolder(LayoutInflater.from(view.context).inflate(R.layout.todo_item, view, false))
    }

    override fun getItemCount(): Int {
        return todos.count()
    }

    override fun onBindViewHolder(holder: ToDoHolder, position: Int) {
        val title = todos[position]
        holder.bindToDo(title)
    }

    inner class ToDoHolder(v: View): RecyclerView.ViewHolder(v), View.OnClickListener {

        var view: View = v
        var title: String = ""

        init {
            v.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            val intent = Intent(view.context, CompleteToDoActivity::class.java)
            intent.putExtra("title", title)
            startActivity(view.context, intent, null)
        }

        fun bindToDo(title: String) {
            this.title = title
            view.titleTextView.text = title
        }

    }
}