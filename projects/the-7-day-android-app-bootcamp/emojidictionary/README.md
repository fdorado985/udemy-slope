#  Emoji Dictionary
Create a basic collection of Emoji, where you will be able to select one and see it on a big size.

Here you will be able to see:
* GridLayout
* RecyclerView
* RecyclerView Adapter
* ViewHolder for RecyclerView
* Activities and Intents
* Send data from one activity to other one

## Demo
![emoji-dictionary-demo](.screenshots/emoji-dictionary-demo.gif)
