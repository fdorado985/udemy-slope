package com.dorado.emoji_dictionary

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //lateinit var layoutManager: LinearLayoutManager
    lateinit var layoutManager: GridLayoutManager
    lateinit var adapter: RecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Configure the layout manager (How the cells are gonna look like)
        //layoutManager = LinearLayoutManager(this)
        layoutManager = GridLayoutManager(this, 3)
        recyclerView.layoutManager = layoutManager

        // Configure the adapter that works as the data source
        adapter = RecyclerAdapter(arrayListOf("😎", "🌎", "‍💻", "👀", "😍"))
        recyclerView.adapter = adapter
    }
}
