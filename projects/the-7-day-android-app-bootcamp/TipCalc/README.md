#  Tip Calc
Create your first Kotlin app, this is a simple Tip Calculator that will let you to see the total and the tip amount of a bill.

Here you will be able to see:
* Elements on Activity Views.
* Id's
* How to reference an element from a view to the class code.
* Functions
* Strings
* ClickListeners on Buttons.

## Demo
![tip-calc-demo](.screenshots/tip-calc-demo.gif)
