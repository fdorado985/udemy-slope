<p align="center">
  <img src=".assets/logo.png" title="Logo">
</p>

---

[![twitter](.assets/twitter.png)](https://twitter.com/fdorado985)
[![instagram](.assets/instagram.png)](https://www.instagram.com/juan_fdorado)

This repo has a big collection of apps that have been created through the years from several Udemy courses.

Here are the projects and a little description of what are they made it for.

Enjoy it and most important KEEP CODING! 👨🏾‍💻

All of this project has been worked day by day, month by month learning new stuffs from iOS Development (maybe a couple from other Technologies) from different sources.

So this is a good big collection! 👍

## Table of Contents
* [Intermediate iOS 10 (Advance your skills)](#intermediate-ios-10-advance-your-skills)
* [Intermediate iOS 11 (Complex and advanced)](#intermediate-ios-11-complex-and-advanced)
* [A Complete Guide To Lean Controllers in iOS](#a-complete-guide-to-lean-controllers-in-ios)
* [MVVM Design Pattern Using Swift in iOS](#mvvm-design-pattern-using-swift-in-ios)
* [Blockchain Programming In iOS Using Swift](#blockchain-programming-in-ios-using-swift)
* [The 7 Day Android App Bootcamp](#the-7-day-android-app-bootcamp)

## Intermediate iOS 10 (Advance your skills)
* [Functional-Programming](projects/intermediate_ios10_advance_your_skills/functional-programming) - Here you're gonna see about Functional Programming and some functions that covers this.
* [My-Address-Book](projects/intermediate_ios10_advance_your_skills/MyAddressBook) - On this project you'll be able to see an address book using `Realm` Database.
* [Tip-Tip](projects/intermediate_ios10_advance_your_skills/tiptip) - Basic tip calculator where you see how to work with `Localization` using `storyboards` and `Localizable.string` files.

## Intermediate iOS 11 (Complex and advanced)
* [Crypto-Price-Tracker](projects/intermediate_ios11_complex_and_advanced_iphone_apps/crypto-tracker) - Get a list of cryptocurrencies... you will see how to call an `API`, `report a PDF`, `save data`, use `TouchID` and `FaceID`, no use `storyboards`.
* [News-Fun](projects/intermediate_ios11_complex_and_advanced_iphone_apps/news-fun) - This app will work with `Alamofire`, `JSON parsing` and most important... `CoreML`.
* [Furniture-Me](projects/intermediate_ios11_complex_and_advanced_iphone_apps/furniture-me) - On this app you will see a Chair using `Augmented Reality`, you'll be able to `scale it`,  `rotate it`,  `move it`.
* [Journal-Day](projects/intermediate_ios11_complex_and_advanced_iphone_apps/day_one_clone) - Simple clone of `Day One` app... you will be able to see `Animations`, `Realm` and a `UISplitViewController`.
* [Sketchpad](projects/intermediate_ios11_complex_and_advanced_iphone_apps/sketchapp) - A simple drawing app where you'll see how to `share` with `UIActivityViewController`, use `UICollectionViews`, `Touch Handler`, add `Color Picker`.

## A Complete Guide To Lean Controllers in iOS
* [Complete Guide](projects/complete_guide_to_lean_controllers) - Learn what is a `Massive View Controller` and what is `Lean Controller`
* [Grocery](projects/complete_guide_to_lean_controllers/grocery) - Take a look to a `Massive View Controller` app.
* [Grocery-Lean-Controller](projects/complete_guide_to_lean_controllers/grocery_lean_controller) - With the original [Grocery](projects/complete_guide_to_lean_controllers/grocery) app see a refactor to be `Lean Controller`.
* [Better-Segues](projects/complete_guide_to_lean_controllers/better_segues) - Look how the common `segues` are used in a good way.
* [Modern-Segues](projects/complete_guide_to_lean_controllers/modern_segues) - Look a refactor to work with a modern and better `segues` way.
* [Secure-Tabs](projects/complete_guide_to_lean_controllers/secure_tabs) - The better way to handle a `LoginViewController` with `Lean View Controllers`.
* [Networking](projects/complete_guide_to_lean_controllers/networking) - How to refactor and handle a `Network` call.
* [Building-UIControl-Extensions](projects/complete_guide_to_lean_controllers/building_uicontrols_extension) - How to work with `UIButton | Layout View` extensions.

## MVVM Design Pattern Using Swift in iOS
* [HelloMVVM](projects/mvvm_design_pattern/HelloMVVM) - Look the modern `Design Pattern` to Populate a `UITableView`
* [MVVM-Binding](projects/mvvm_design_pattern/mvvm_binding) - Take a look of how implement `View` to `View Model Binding`
  * [MVVM-Binding-Full](projects/mvvm_design_pattern/mvvm_binding_full)
* [MVVM-Binding-Validations-Basic](projects/mvvm_design_pattern/mvvm_binding_validations_basic) - Implementing Basic Validations.
* [MVVM-Binding-Validations-Ruled-Based](projects/mvvm_design_pattern/mvvm_binding_validations_ruled_based) - Implementing Rule Based Validations.
* [MVVM-Networking](projects/mvvm_design_pattern/flowers_app_networking) - Implementing Networking Layer and call it from View Model.
* [HeadlinesApp-MVC](projects/mvvm_design_pattern/HeadlinesApp_mvc) - Original MVC app to refactor it to MVVM.
* [HeadlinesApp-MVVM](projects/mvvm_design_pattern/HeadlinesApp_mvvm) - Refactor of MVC to MVVM pattern.

## Blockchain Programming In iOS Using Swift
* [Understanding Blockchain Technology](https://medium.com/@fdorado985/blockchain-technology-c9fc935ccca7) - Learn the basics of the Blockchain Technology.
* [Implementing Blockchain Core Engine](projects/blockchain_programming_in_ios_using_swift/implementing_blockchain_core_engine) - See how the Blockchain core works.

## The 7 Day Android App Bootcamp
* [Kotlin Fun](projects/the-7-day-android-app-bootcamp/kotlinfun) - Learn the basics of Kotlin Language.
* [Tip Calc](projects/the-7-day-android-app-bootcamp/TipCalc) - Create your first Android App with Kotlin to calculate the tip.
* [Emoji Dictionary](projects/the-7-day-android-app-bootcamp/emojidictionary) - Create a table with some emojis on it.
* [To-Do List](projects/the-7-day-android-app-bootcamp/ToDoList) - Create a simple To-Do List app.

## Design Patterns In iOS Using Swift
* [Model-View-Controller](projects/design_patterns_in_ios_using_swift)
* [Delegation](projects/design_patterns_in_ios_using_swift)
* [Strategy](projects/design_patterns_in_ios_using_swift)
* [Singleton](projects/design_patterns_in_ios_using_swift)
* [Observer](projects/design_patterns_in_ios_using_swift)
* [Builder](projects/design_patterns_in_ios_using_swift)
* [Model-View-ViewModel](projects/design_patterns_in_ios_using_swift)
* [Factory](projects/design_patterns_in_ios_using_swift)
* [Adapter](projects/design_patterns_in_ios_using_swift)
* [Iterator](projects/design_patterns_in_ios_using_swift)
